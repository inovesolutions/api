const functions = require('firebase-functions');
const admin = require('./firebaseAdmin');
const _ = require('underscore');
const plural = require("pluralize-ptbr");
const moment = require("moment-timezone");
const uuid = require('uuid/v1');

let messages = {};

messages.post = (request, response) => {

    if (!request.body.message) return response.status(400).send({
        code: "400.10",
        reason: "O campo message é obrigatório"
    });

    request.body.message.moderate = { authorized: true };
    
    admin.database().ref("users").child(request.user.uid)
    .once("value", (senderSnap) =>{ 
        if(senderSnap.exists()){

            let sender = senderSnap.val();

            request.body.message.content.header.senderId = request.user.uid;
            request.body.message.content.header.senderName = sender.profile.displayName;
            request.body.message.content.header.senderAvatar = sender.profile.avatar || "";
            request.body.message.content.header.date = moment.tz("America/Sao_Paulo").toISOString();

            let reference = admin.database().ref("messages").push();
            reference.set(request.body.message, (error) => {
                if (!error){

                    let updates = {};
                    let jobId = uuid();
                    let count = 0;

                    for(let receiverId in request.body.message.receivers){
                        updates[`jobmessage/${jobId}/${reference.key}/receivers/${receiverId}`] = 
                        request.body.message.receivers[receiverId];

                        count = count + 1;
                        if(count === 100){
                            jobId = uuid();
                            count = 0;
                            admin.database().ref().update(updates);
                            updates = {};
                        }
                    }                    
                    
                    if(count < 100){
                        admin.database().ref().update(updates);
                    }

                    return response.status(200).send({
                        messageId: reference.key
                    });

                } else return response.status(400).send({
                    code: "400.20",
                    reason: "Não foi possível criar a mensagem",
                    error: error
                });
            });
        }
        else return response.status(401).send({
            code: "401.10",
            reason: "Não autorizado"
        });
    });
    
}

messages.getMessage = (request, response) => {
    console.log(request.params.messageId);
    admin.database().ref("users").child(request.user.uid)
    .child("index").child("messages").child(request.params.messageId)
    .once("value", snapshot => {
        if(!snapshot.exists()) return response.status(401).send("NOT ALLOWED");
        admin.database().ref("messages").child(request.params.messageId)
        .once("value", messages => {
            if(messages.exists()) response.status(200).send(messages.val());
            else response.status(404).send("NOT FOUND");
        });
    });
};

messages.getUsers = (request, response) => {
    let tagId = request.params.tagId;
    admin.database().ref("contacts").child(request.user.uid).child("withTags").child(tagId)
    .once("value", hasContact => {
        if(!hasContact.exists()) return response.status(401).send("NOT ALLOWED");
        admin.database().ref("tags").child(tagId)
        .once("value", tag => {
            if(tag.exists()) response.status(200).send(tag.val());
            else response.status(404).send("NOT FOUND");
        });
    });
}

module.exports = messages;