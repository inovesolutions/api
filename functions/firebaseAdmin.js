let functions = require('firebase-functions');
let admin = require('firebase-admin');

let serviceAccount = require('./serviceAccountKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://olaescola-170512.firebaseio.com",
    storageBucket: "olaescola-170512.appspot.com"
});

module.exports = admin;