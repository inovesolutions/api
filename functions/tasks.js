let functions = require('firebase-functions');
let gcs = require('@google-cloud/storage')();
let admin = require('./firebaseAdmin');
let bucket = admin.storage().bucket("colegiouirapuru-b9be5.appspot.com");
let config = require('./config');
const plural = require("pluralize-ptbr");

module.exports.deleteGroups = functions.database.ref("/tasks/deleteGroups")
  .onWrite(event => {
    return admin.database().ref("tags").child("COLEGIO UIRAPURU").once("value", usersSnapshot => {
      let users = usersSnapshot.val();
      let updates = {};
      for (let userId in users) {
        updates[`users/${userId}/profile/groups`] = null;
      }

      updates["groups"] = null;
      admin.database().ref().update(updates, error => {
        if (error) console.log(error);
      });
    });
  });

module.exports.organizeDatabase = functions.database.ref("/tasks/organizeDatabase")
.onWrite( event => {
  admin.database().ref("jbomessage").remove();
  admin.database().ref("jobmessage").remove();
  admin.database().ref("jobmail").remove();
  admin.database().ref("importAvatars").remove();
})

module.exports.updateUsers = functions.database.ref("tasks/updateUsers")
  .onWrite(event => {
    return admin.database().ref("tags").child("COLEGIO UIRAPURU").once("value", usersSnapshot => {
      let users = Object.keys(usersSnapshot.val());
      let index = 0;
      admin.database().ref("tasks").child("onUpdateUser").on("child_removed", data => {
        index = index + 1;
        if (index === users.length) {
          admin.database().ref("tasks").child("onUpdateUser").off("child_removed");
          return "Tags atualizadas";
        }
        admin.database().ref("tasks").child("onUpdateUser").child(users[index]).set(true);
      });
      admin.database().ref("tasks").child("onUpdateUser").child(users[index]).set(true);
    });
  });

module.exports.onUpdateUser = functions.database.ref("tasks/onUpdateUser/{userId}")
  .onWrite(event => {
    let updates = {};
    return admin.database().ref("users").child(event.params.userId)
      .once("value", (userDataSnapshot) => {
        if (userDataSnapshot.exists()) {
          let user = userDataSnapshot.val();
          let userId = event.params.userId;
          for (let tagId in user.tags) {
            updates[`tags/${tagId}/${userId}`] = user.profile;
            for (let relationId in user.relations) {
              let relation = user.relations[relationId];
              if (relation.types) {
                relation.types.forEach(type => {
                  if(type === "Pai" || type === "Mae"){
                    updates[`users/${relationId}/tags/${plural(type)} de ${tagId}`] = true;
                  }
                });
              }
            }
          }

          admin.database().ref().update(updates, error => {
            if (error) return console.log(error);
            event.data.ref.set(null);
          });
        }
      });
  });

module.exports.addAllTagsToContact = functions.database.ref("tasks/addAllTagsToContact")
  .onWrite(event => {
    if (!event.data.exists()) return;
    let destinations = event.data.val();
    if (!destinations instanceof Array) return;

    return admin.database().ref("tags").once("value", tags => {
      if (!tags.exists()) return;
      let updates = {};
      destinations.forEach(destination => {
        for (let tagId in tags.val()) {
          let tag = tags.val()[tagId];
          updates[`contacts/${destination}/withTags/${tagId}`] = true;
          updates[`users_contacts/${destination}`] = updates[`users_contacts/${destination}`] || {};
          updates[`users_contacts/${destination}`] = Object.assign(updates[`users_contacts/${destination}`], tag);
        }
      });
      admin.database().ref().update(updates);
      event.data.ref.remove();
    });
  });

  module.exports.addContacts = functions.database.ref("tasks/addContacts")
  .onWrite(event => {
    if (!event.data.exists()) return;
    let destinations = event.data.val().destinations;
    let sources = event.data.val().sources;
    if (!destinations instanceof Array) return;

    return admin.database().ref("tags").once("value", tags => {
      if (!tags.exists()) return;
      let updates = {};
      destinations.forEach(destination => {
        let users = tags.val()[destination];
        for(let userId in users){
          sources.forEach(source => {
            let contacts = tags.val()[source];
            updates[`contacts/${userId}/withTags/${source}`] = true;
            updates[`users_contacts/${userId}`] = updates[`users_contacts/${userId}`] || {};
            updates[`users_contacts/${userId}`] = Object.assign(updates[`users_contacts/${userId}`], contacts);
          });
        }
      });
      admin.database().ref().update(updates);
      event.data.ref.remove();
    });
  });