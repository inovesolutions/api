let chats = require('./chats');
let messages = require('./messages');
let auth = require('./auth');
let users = require('./users');
let api = require('./api');
let tasks = require('./tasks');

//Funções do chat
//exports.onChatChange = chats.onChatChange;
exports.onChatMessageRead = chats.onChatMessageRead;
exports.onNewChatMessage = chats.onNewMessage;
//exports.onUserWriting = chats.onUserWriting;

//Funções de mensagens
exports.onSendMessage = messages.onSendMessage;
exports.onSendPush = messages.onSendPush;
exports.removeMessageFromUser = messages.removeMessageFromUser;

exports.onSendEmail = messages.onSendEmail;
exports.onSendGridWebhook  = messages.onSendGridWebhook;

exports.onSendSMS = messages.onSendSMS
exports.onZenviaCallback = messages.onZenviaCallback;

exports.onReceivedMessage = messages.onReceivedMessage;
exports.onReadMessage = messages.onReadMessage;
exports.getMessage = messages.getMessage;
exports.onNewMessage = messages.onNewMessage;

exports.verifyPhoneNumber = auth.verifyPhoneNumber;
exports.verifyEmail = auth.verifyEmail;
exports.verifyWithCredentials = auth.verifyWithCredentials;

//Funções do Usuário
exports.onProfileChange = users.onProfileChange;
exports.onUserTagCreated = users.onUserTagCreated;
exports.onUserTagDeleted = users.onUserTagDeleted;
exports.onRelationCreated = users.onRelationCreated;
exports.onRelationDeleted = users.onRelationDeleted;

exports.api = api;

exports.deleteGroups = tasks.deleteGroups;
exports.addAllTagsToContact = tasks.addAllTagsToContact;
exports.updateUsers = tasks.updateUsers;
exports.onUpdateUser = tasks.onUpdateUser;
exports.organizeDatabase = tasks.organizeDatabase;
exports.addContacts = tasks.addContacts;