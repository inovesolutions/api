// Importações
let config = require('./config');
let push = require('./push');
const pug = require('pug');
const moment = require("moment-timezone");

// Funções do FireBase
let functions = require('firebase-functions');
let gcs = require('@google-cloud/storage')();
let admin = require('./firebaseAdmin');
let bucket = admin.storage().bucket("colegiouirapuru-b9be5.appspot.com");
// Configuração para fazer chamadas rest
let unirest = require('unirest');
// Configurações do Google Shortner
var GoogleUrl = require('google-url');
googleUrl = new GoogleUrl();
googleUrl = new GoogleUrl( { key: config.google.shortener.key });

module.exports.onNewMessage = functions.database.ref("/messages/{messageId}")
.onCreate(event => {
    
    let message = event.data.val();
    admin.database()
    .ref(`users/${message.content.header.senderId}/index/messages`)
    .child(event.params.messageId).set({
        content: message.content
    });

});

module.exports.onSendPush = functions.database.ref("/users/{userId}/messages/{messageId}/content/header/pushId")
.onWrite(event => {
    if(!event.data.exists()) return;
    return event.data.adminRef.root.child("messages").child(event.params.messageId)
    .once("value", (messageSnapshot) =>{
        if(!messageSnapshot.exists()) return;
        let message = messageSnapshot.val();
        push.sendFromMessage([event.data.val()], 1, message)
        .then(value => {
            registerHistory(event.params.userId, event.params.messageId, [{
                source: "app",
                event: "sent",
                eventDate: new Date().toISOString(),
                type: "info",
                detail: "Push notification enviado",
                result: value
            }]);
        })
        .catch(error => {
            registerHistory(event.params.userId, event.params.messageId, [{
                source: "app",
                event: "sent",
                eventDate: new Date().toISOString(),
                type: "error",
                detail: "Push notification não foi enviado",
                result: error
            }]);
        });
    });
    
});

module.exports.removeMessageFromUser = functions.database.ref("messages/deleted/{messageId}")
.onCreate(event => {
    admin.database().ref(`messages/${event.params.messageId}`)
    .once("value", messageSnapshot => {
        if(messageSnapshot.exists()){
            for(userId in messageSnapshot.val().receivers){
                admin.database()
                .ref(`users/${userId}/messages/${event.params.messageId}/deleted`).set(true);
            }
        }
    });
});

module.exports.onSendMessage = functions.database.ref("/jobmessage/{jobId}/{messageId}/receivers/{userId}")
.onCreate(event => {

    if(!event.data.exists()) return;

    return event.data.adminRef.root.child("messages").child(event.params.messageId)
    .once("value", (messageSnapshot) =>{

        if(!messageSnapshot.exists()) return;
        let message = messageSnapshot.val();
        if(!message.moderate.authorized) return;

        let updates = {};
        let userId = event.params.userId;
        let content = Object.assign({}, message.content);
        let receiver = message.receivers[userId];

        if(receiver.relations){
            content.header.relations = receiver.relations;
        }

        content.header.date = moment.tz("America/Sao_Paulo").toISOString();

        // if(receiver.pushId){
        //     content.header.pushId = receiver.pushId
        // }

        updates[`/users/${userId}/messages/${event.params.messageId}`] = {
            content: content,
            read: false,
            received: false,
            deleted: false
        };

        updates[`/messages/${event.params.messageId}/receivers/${userId}/appStatus`] = {
            eventDate:new Date().toISOString(),
            event: 'sent'
        };

        let historyId = admin.database().ref("messages").child(event.params.messageId)
        .child("receivers").child(userId).child("history").push().key;

        updates[`/messages/${event.params.messageId}/receivers/${userId}/history/${historyId}`] = {
            source: "app",
            event: "sent",
            eventDate: new Date().toISOString(),
            type: "info",
            detail: "Mensagem enviada para o APP"
        }
        
        updates[`/messages/${event.params.messageId}/receivers/${userId}/moderate/authorized`] = true;
        updates[`/users/${userId}/unread`] = 1;

        admin.database().ref().update(updates, () => {
            var longURL = `${config.env.cloudFunctionsUrlBase}/getMessage?m=${event.params.messageId}.-userId-.email`;
            var mailEnvelope = 
                {
                    personalizations:[
                        {
                            to:[{ email: receiver.email, name: receiver.name}],
                            custom_args: { messageId: event.params.messageId, userId: userId },
                            substitutions : { '-userId-': userId, '-name-': receiver.name }
                        }
                    ],   
                    from: { email:config.sendGrid.from },
                    subject: `${config.env.appname} - Novas mensagens!`,
                    content: [{
                        type: 'text/html',
                        value: `<h3>Olá -name-,</h3> 
                                <p>Você tem uma nova mensagem de ${config.env.appname}.</p>
                                <p><b>O aplicativo do Colégio Uirapuru tem uma nova versão para 2018, atualize para receber as novidades. <br/>As mensagens contidas na versão antiga poderão ser reenviadas a pedido. Faça uso do chat para estreitarmos nossa comunicação.</b></p>
                                <h3>Baixe o aplicativo no App Store ou no Google Play</h3>
                                <br/>Para ver a mensagem, <a href="${longURL}">clique aqui</a>.`
                    }]
                };
        
            
            event.data.ref.set(null);

            // unirest.post(config.sendGrid.url)
            // .headers({
            //     'Accept': 'application/json', 
            //     'Content-Type': 'application/json',
            //     'Authorization': `Bearer ${config.sendGrid.key}`
            // })
            // .send(mailEnvelope)
            // .end(function (response) {
            //     if  (response.code === 'ETIMEDOUT'){        
            //         registerHistory(userId, event.params.messageId, [{
            //             source: "email",
            //             event: "sent",
            //             eventDate: new Date().toISOString(),
            //             type: "error",
            //             detail: JSON.stringify(response.body)
            //         }]);
            //     }else if (response.code >=200 && response.code<300){
            //         registerHistory(userId, event.params.messageId, [{
            //             source: "email",
            //             event: "sent",
            //             eventDate: new Date().toISOString(),
            //             type: "info",
            //             detail: "Mensagem registrada para envio de email"
            //         }]);
            //     }else{
            //         registerHistory(userId, event.params.messageId, [{
            //             source: "email",
            //             event: "sent",
            //             eventDate: new Date().toISOString(),
            //             type: "error",
            //             detail: JSON.stringify(response.body)
            //         }]);
            //     }
            // });  
        });
    });
});

/*-------------------------------------------------------------------------------------*/
module.exports.onSendEmail = functions.database.ref("/jobmail/{jobMailId}")
.onWrite((event) => {

    if(!event.data.exists()) return;
    let item = event.data.val();
    if(!item.receivers) return event.data.ref.set(null);

    let toReceivers = [];
    let toSubstitutions = {};
    let index = 0;
    let keys = Object.keys(item.receivers);

    let sendLimit = config.sendGrid.sendLimit;
    if(keys.length < sendLimit){
        sendLimit = keys.length;
    }

    do{
        let userId = keys[index];
        let receiver = item.receivers[userId];
        toReceivers.push({
            to:[{
                email: receiver.email,
                name: receiver.name
            }],
            custom_args: {
                messageId: item.messageId,
                userId: userId
            },
            substitutions : {
                '-userId-': userId,
                '-name-': receiver.name
            }
        });

        delete item.receivers[userId];

    } while (++index < sendLimit);

    var longURL = `${config.env.cloudFunctionsUrlBase}/getMessage?m=${item.messageId}.-userId-.email`;
    var mailEnvelope = 
        {
            personalizations:toReceivers,   
            from: {
                email:config.sendGrid.from
            },
            subject: `${config.env.appname} - Novas mensagens!`,
            content: [{
                type: 'text/html',
                value: `<h3>Olá -name-,</h3> 
                        <p>Você tem uma nova mensagem de ${config.env.appname}.</p>
                        <p><b>O aplicativo do Colégio Uirapuru tem uma nova versão para 2018, atualize para receber as novidades. <br/>As mensagens contidas na versão antiga poderão ser reenviadas a pedido. Faça uso do chat para estreitarmos nossa comunicação.</b></p>
                        <h3>Baixe o aplicativo no App Store ou no Google Play</h3>
                        <br/>Para ver a mensagem, <a href="${longURL}">clique aqui</a>.`
            }]
        };

    unirest.post(config.sendGrid.url)
    .headers({
        'Accept': 'application/json', 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${config.sendGrid.key}`
    })
    .send(mailEnvelope)
    .end(function (response) {

        if  (response.code === 'ETIMEDOUT'){        
            console.error(`SendGridTimeout Header: ${JSON.stringify(response.headers)}`);
            console.error(`SendGridTimeout Body: ${JSON.stringify(response.body)}`);
        }else if (response.code >=200 && response.code<300){
            event.data.ref.set(item);
        }else{
            console.error(`SendGridError Header: ${JSON.stringify(response.headers)}`);
            console.error(`SendGridError Body: ${JSON.stringify(response.body)}`);
        }
    });   
});

/*-------------------------------------------------------------------------------------*/
exports.onSendGridWebhook = functions.https.onRequest((request, response)=>{
    
    if(request.body && request.body.length > 0 && !request.body[0].messageId){
        return response.status(400).send("BAD REQUEST");
    }

    request.body.forEach(function(item){
        
        let emailStatus = {
            eventDate: new Date(item.timestamp*1000).toISOString(),
            event : "undelivered"
        }
        let emailError = false;

        let histories = [{
            source: "email",
            event: item.event,
            eventDate: new Date(item.timestamp*1000).toISOString(),
            type: "info",
            detail: "Mensagem enviada por email"
        }];

        switch (item.event) {
            case 'dropped':
                emailError = true;
                histories[0].detail = `Email - Não foi possível enviar a mensagem. (${item.reason})`;
                histories[0].type = "error";
                break;
            case 'deferred':
                emailError = true;
                histories[0].detail = `Email - A mensagem foi negada pelo destinatário. (${item.response})`;
                histories[0].type = "error";
                break;
            case 'bounce':
                emailError = true;
                histories[0].detail = `Email - O envio da mensagem foi suspenso e aguarda revisão. (${item.status} - ${item.reason})`;
                histories[0].type = "warning";
                break;
            case 'delivered':
                emailStatus.event = "delivered";
                histories[0].detail = "Email - Mensagem entregue";
                break;
            case 'open':
                emailStatus.event = "delivered";
                histories[0].detail = "Email - Mensagem aberta pelo destinatário";
                break;            
            case 'processed':
                emailStatus.event = "sent";
                histories[0].detail = "Email - Mensagem enviada";
                break;
            default: // delivered or opened
        }

        registerHistory(item.userId,item.messageId,histories);

        // if (emailError) {
        //     admin.database().ref("users").child(item.userId).child("profile").child("mobilePhone")
        //     .once("value", (mobilePhoneSnapshot) =>{
        //         let mobilePhone = mobilePhoneSnapshot.val();
        //         if(mobilePhone){

        //             let receiver = {};
        //             receiver[item.userId] = { mobilePhone: mobilePhone };

        //             admin.database().ref("jobsms")
        //                 .push({ receivers: [receiver], messageId: messageId });
        //         }
        //         else{
        //             console.error(`Não foi possível enviar o sms, pois o destinatário não possui celular cadastrado. userId=${item.userId}`);
        //             registerHistory(item.userId,item.messageId,[{
        //                     source: "sms",
        //                     event: "dropped",
        //                     eventDate: new Date().toISOString(),
        //                     type: "warning",
        //                     detail: "Não foi possível enviar o sms, pois o destinatário não possui celular cadastrado."
        //             }]);
        //         }

        //     });
            
        // }

       
        let reference = admin.database().ref("messages").child(item.messageId).child("receivers")
        .child(item.userId).child("emailStatus");
        updateStatus(reference, emailStatus);

    });

    return response.status(200).send("OK");

});

/*-------------------------------------------------------------------------------------*/
module.exports.onSendSMS = functions.database.ref("/jobsms/{jobSMSId}")
.onWrite((event) => {

    if(!event.data.exists()) return;
    let item = event.data.val();
    if(!item.receivers) return event.data.ref.set(null);

    let zenvia = {
        sendSmsMultiRequest:{
            sendSmsRequestList: []
        }
    };

    let keys = Object.keys(item.receivers);
    let sendLimit = config.zenvia.sendLimit;
    if(keys.length < sendLimit){
        sendLimit = keys.length;
    }

    let index = 0;
    let proccessed = 0;

    do{

        let userId = keys[index];
        let receiver = item.receivers[userId];
        var longURL = `${config.env.cloudFunctionsUrlBase}/getMessage?m=${item.messageId}.${userId}.sms`;
        googleUrl.shorten(longURL, (error, shortUrl) =>{
            
            if(error){
                registerHistory(userId, item.messageId, [{
                    source: "sms",
                    event: "error",
                    eventDate: new Date().toISOString(),
                    type: "error",
                    detail: "Não foi possível gerar a url curta para enviar o sms"                    
                }]);
            }
            else{
                zenvia.sendSmsMultiRequest
                .sendSmsRequestList.push({
                    from: config.env.appname,
                    to: `55${receiver.mobilePhone}`,
                    msg: `Novas mensagens de ${config.env.appname}, ${shortUrl}`,
                    callbackOption: "ALL",
                    schedule: new Date().toISOString(),
                    id: `${item.messageId}.${userId}`
                });
            }

            delete item.receivers[userId];

            if(++proccessed === sendLimit 
                && zenvia.sendSmsMultiRequest.sendSmsRequestList.length > 0){
                unirest.post(config.zenvia.urlMultiple)
                .headers({
                    "Authorization": `Basic ${config.zenvia.base64}`,
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                })
                .send(zenvia)
                .end(function (response) {
                    if  (response.code === 'ETIMEDOUT'){
                        // timeout de conexao - abre chamado            
                        console.error(`ZenviaTimeout Header: ${JSON.stringify(response.headers)}`);
                        console.error(`ZenviaTimeout Body: ${JSON.stringify(response.body)}`);
                    }
                    else if (response.code >=200 && response.code<300){
                        event.data.ref.set(item);
                    }else{
                        // Problemas nos servidores ou na mensagem, para a operação e grava o chamado
                        console.error(`ZenviaError Header: ${JSON.stringify(response.headers)}`);
                        console.error(`ZenviaError Body: ${JSON.stringify(response.body)}`);
                    }
                });
            }

        });

    }while(++index < sendLimit);

    
});

module.exports.onZenviaCallback = functions.https.onRequest((request, response) =>{

    if(request.body.callbackMtRequest){
    
        let status = Number(request.body.callbackMtRequest.status);

        if (status < 2) { //0=ok or 1=scheduled 
            return response.status(200).send("OK");
        }

        let messageId = request.body.callbackMtRequest.id.split('.')[0];
        let userId = request.body.callbackMtRequest.id.split('.')[1];
        
        let smsStatus = {
            eventDate: new Date().toISOString(),
            event : "undelivered" // sent, delivered, read ou undelivered
        }
        
        let history = {
            source: "sms",
            event: request.body.callbackMtRequest.statusMessage, // sent, delivered, read ou undelivered
            eventDate: new Date(item.timestamp).toISOString(),
            //type: "info",
            detail: `${request.body.callbackMtRequest.statusDetailMessage } (${request.body.callbackMtRequest.statusDetail})`
        };

        switch (status) 
        {
            case 2 : //sent
                smsStatus.event = "sent";
                history.type = "info";
                break;
            case 3 : //delivered
                smsStatus.event = "delivered";
                history.type = "info";
                break;
            case 4 :  // Not received
                history.type = "warning";
                break;
            case 5 :  // Blocked - No Coverage
                history.type = "warning";
                break;
            case 6 :  // Blocked - Black listed
                history.type = "warning";            
                break;
            case 7 :  // Blocked - Invalid Number
                history.type = "error";
                break;
            case 8 :  // Blocked - Content not allowed
                history.type = "error";            
                break;
            case 8 :  // Blocked - Message Expired
                history.type = "error";            
                break;  
            case 9 :  // Blocked
                history.type = "warning";            
                break;
            case 10 : // Error
                history.type = "error";            
                break;
            default : // Unknow
                history.type = "warning";
                break;
        }

        registerHistory(userId,messageId,[history]);
        let reference = admin.database().ref("messages").child(messageId).child("receivers")
        .child(userId).child("smsStatus");
        updateStatus(reference, smsStatus);        
        
        return response.status(200).send("OK");
    }
    else{
        console.error("Retorno de callback do Zenvia fora de protocolo definido!");
        return response.status(400).send("BAD REQUEST");
    }
});

module.exports.onReceivedMessage = functions
    .database.ref("/users/{userId}/messages/{messageId}/received")
    .onUpdate((event)=>{

        if(event.data.exists() && event.data.val() == true){

            registerHistory(event.params.userId, event.params.messageId, [{
                detail: "Mensagem recebida pelo APP",
                event: "delivered",
                eventDate: new Date().toISOString(),
                type: "info",
                source: "app"
            }]);

            let reference = event.data.adminRef.root
                .child("messages").child(event.params.messageId)
                .child("receivers").child(event.params.userId).child("appStatus");

            updateStatus(reference, {
                event: "delivered",
                eventDate: new Date().toISOString()
            });
        }

    });

module.exports.onReadMessage = functions.database.ref("/users/{userId}/messages/{messageId}/read")
.onUpdate((event)=>{

    if(event.data.exists() && event.data.val() == true){
        registerHistory(event.params.userId, event.params.messageId, [{
            detail: "Mensagem lida pelo APP",
            event: "read",
            eventDate: new Date().toISOString(),
            type: "info",
            source: "app"
        }]);

        let reference = event.data.adminRef.root
            .child("messages").child(event.params.messageId)
            .child("receivers").child(event.params.userId).child("appStatus");

        admin.database().ref().child("users").child(event.params.userId)
        .child("unread").transaction((unread)=>{
            if(unread && unread > 0) unread--;            
            return (unread && unread > 0) ? unread : 0;
        });

        updateStatus(reference, {
            event: "read",
            eventDate: new Date().toISOString()
        });
    }

});

module.exports.getMessage = functions.https.onRequest((request, response)=>{

    if (request.headers.referer) { return response.status(200).send(`OK`); }

    console.log(request.query.m);
    let messageId = request.query.m.split(".")[0];
    let userId = request.query.m.split(".")[1];
    let from = request.query.m.split(".")[2];

    // resgata a mensagem do banco
    admin.database().ref("messages").child(messageId)
    .once("value", (messageSnapshot)=>{

        if(!messageSnapshot.exists()) return response.status(404).send("Mensagem não encontrada");

        let message = messageSnapshot.val();

        if(from === "email" && !message.receivers[userId].emailStatus){
            console.error("getMessage: A requisicao do google não enviou a cabecalho referer, verificar o codigo para readaptar as mudanças");
            return response.status(200).send("OK");
        } 

        if(from === "sms" && !message.receivers[userId].smsStatus){ 
            console.error("getMessage: A requisicao do google não enviou a cabecalho referer, verificar o codigo para readaptar as mudanças");
            return response.status(200).send("OK"); 
        }

        var webMessage = {
            content: message.content.body.content,
            title: message.content.body.title,
            subtitle: message.content.body.subtitle,
            moduleId: message.content.header.moduleId,
            messageDate: message.content.header.date,
            messageId: messageId,
            userId: userId,
            from: from
        }

        admin.database().ref('users').child(message.content.header.senderId).once('value', (senderSnapshot) =>{

            let receiver = message.receivers[userId];
            webMessage.senderName = senderSnapshot.val().profile.name;
            webMessage.senderAvatar = senderSnapshot.val().profile.avatar;
            webMessage.relations = receiver.relations;
            response.setHeader("Content-Type", "text/html");
            response.status(200).send(startWebRender(webMessage));
            
        });

    });

});

function startWebRender(webMessage){
    let html = "";
    switch (webMessage.moduleId){
        default:
            html = pug.renderFile("templates/message.pug", { message : webMessage });
    }
    if(webMessage.from === "email"){

        console.log("Mensagem lida por Email")
        registerHistory(webMessage.userId, webMessage.messageId, [{
            detail: "Mensagem lida por Email",
            event: "read",
            eventDate: new Date().toISOString(),
            type: "info",
            source: "email"
        }]);

        let reference = admin.database().ref("messages").child(webMessage.messageId).child("receivers")
        .child(webMessage.userId).child("emailStatus");

        updateStatus(reference, {
            eventDate: new Date().toISOString(),
            event: "read"
        });

    }
    else if (webMessage.from === "sms"){
        let reference = admin.database().ref("messages").child(webMessage.messageId).child("receivers")
        .child(webMessage.userId).child("smsStatus");
        updateStatus(reference, {
            eventDate: new Date().toISOString(),
            event: "read"
        });
    }
    return html;
}

function updateStatus(reference, status){
    reference.transaction((lastStatus)=>{
        if(lastStatus && lastStatus.event === "read") return lastStatus;
        else return status;
    });
}

function registerHistory(userId, messageId, histories){
    histories.forEach((history)=>{
        admin.database().ref("messages").child(messageId)
        .child("receivers").child(userId).child("history").push(history);
    });
}
