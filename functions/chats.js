let pushNotification = require("./push");

let functions = require('firebase-functions');
let admin = require('./firebaseAdmin');
let push = require('./push');

module.exports.onChatMessageRead = functions.database
.ref("users/{userIdMe}/chats/{userIdTo}/messages/{messageId}/read")
.onWrite((event)=>{
    if(event.data.exists() && event.data.val()){
        admin.database().ref().child("users").child(event.params.userIdMe)
        .child("chats").child(event.params.userIdTo).child("messages")
        .child(event.params.messageId)
        .once("value", (messageDataSnapshot)=>{
            if(!messageDataSnapshot.exists()) return;
            if(messageDataSnapshot.val().senderId !== event.params.userIdMe){
                admin.database().ref().child("users").child(event.params.userIdMe)
                .child("unread").transaction((unread)=>{
                    if(unread && unread > 0) unread--;
                    return (unread && unread > 0) ? unread : 0;
                });
            }
        });
    }
});

module.exports.onUserWriting = functions.database
.ref("users/{userIdMe}/chats/{userIdTo}/writing")
.onWrite((event) => {
    // if(!event.data.exists() && event.data.val() === true){
    //     event.data.ref.set(false);
    // }
});

module.exports.onNewMessage = functions.database
.ref("users/{userIdMe}/chats/{userIdTo}/messages/{messageId}")
.onCreate((event) => {

    if(!event.data.exists()) return;

    let message = event.data.val();

    return admin.database().ref("users").child(event.params.userIdMe)
    .once("value", (userMeSnapshot) => {
        if(!userMeSnapshot.exists()) return;
        let userMe = userMeSnapshot.val();
        return admin.database().ref("users").child(event.params.userIdTo)
        .once("value", (userToSnapshot) => {
            if(!userToSnapshot.exists()) return;
            let userTo = userToSnapshot.val();
            if(message.senderId === event.params.userIdTo && !message.read){
                let pushIds = [];
                if(userMe.pushId) push.sendFromChat(userTo.profile.displayName, [userMe.pushId], message, 1);
                if(userMe.adminPushId) push.sendFromChat(userTo.profile.displayName, [userMe.adminPushId], message, 1);
                if(pushIds.length > 0) push.sendFromChat(userTo.profile.displayName, pushIds, message, 1);
            }
        });
    });
});
