const functions = require('firebase-functions');
const admin = require('./firebaseAdmin');
const _ = require('underscore');
const plural = require("pluralize-ptbr");
const fs = require('fs');
const utf8 = require('utf8');
let config = require('./config');
let gcs = require('@google-cloud/storage')();
let bucket = admin.storage().bucket("colegiouirapuru-b9be5.appspot.com");

const path = require('path');
const os = require('os');
const spawn = require('child-process-promise').spawn;

let __users = {};

__users.isAdmin = (request, response) => {
    admin.database().ref("users").child(request.user.uid).child("tags").child("ADMIN")
    .once("value", snapshot => {
        if(snapshot.exists() && snapshot.val()) response.status(200).send(true);
        else response.status(200).send(false);
    });
}

__users.list = (request, response) => {
    let startAt = request.body.startAt;
    let limitFirst = request.body.limitFirst;
    admin.database().ref("tags").child("ADMIN").child(request.user.uid)
    .once("value", isAdmin => {
        if(!isAdmin.exists()) return response.status(401).send("NOT ALLOWED");
        admin.database().ref("users").orderByKey()
        .startAt(startAt).limitToFirst(limitFirst).once("value", users => {
            if(users.exists()) response.status(200).send(users.val());
            else response.status(200).send({});
        });
    })
}

__users.getUser = (request, response) => {
    let userId = request.params.userId;
    admin.database().ref("tags").child("ADMIN").child(request.user.uid)
    .once("value", isAdmin => {
        if(!isAdmin.exists()) return response.status(401).send("NOT ALLOWED");
        admin.database().ref("users").child(userId)
        .once("value", user => {
            if(user.exists()) return response.status(200).send(user.val());
            else return response.status(404).send("NOT FOUND");
        });
    });
}


__users.datasync = (request, response) => {

    let errorLog = [];
    
    if(!request.headers.api_key && !request.headers.client_id){
        return response.status(401).send("Unauthorized");
    }
    admin.database().ref()
    .child('integration').once('value', (snapshot) => {
        if(snapshot.exists()){
            let integration = snapshot.val();
            let client = integration.filter((item) => { 
                return item.client_id === request.headers.client_id 
                && item.api_key === request.headers.api_key
            })[0];
            if(!client) return response.status(401).send("O cliente informado não foi encontrado");
            let people =  request.body;
            if(!people){
                console.log("Coleção de pessoas não foi informada, adicione a propriedade people ao corpo da requisição")
                return response.status(400)
                .send("Coleção de pessoas não foi informada, adicione a propriedade people ao corpo da requisição");
            }
            if(!people instanceof Array){
                console.log("Coleção de pessoas deve ser um Array");
                return response.status(400).send("Coleção de pessoas deve ser um Array");
            }
            let importData = {};
            people.forEach((person, index) => {
                if(validate(person)){
                    importData[`/users/${person.importId}/profile/displayName`] = person.displayName;
                    importData[`/users/${person.importId}/profile/gender`] = person.gender;
                    importData[`/users/${person.importId}/profile/email`] = person.email;
                    importData[`/users/${person.importId}/profile/mobilePhone`] = person.mobilePhone;
                    importData[`/users/${person.importId}/profile/avatar`] = person.avatar;
                    if(person.relations) {
                        importData[`/users/${person.importId}/relations`] = person.relations;
                    }
                    if(!person.tags) person.tags = [config.env.appname];
                    else person.tags.push(config.env.appname);
                    person.tags.forEach((tag) => {
                        importData[`/users/${person.importId}/tags/${tag}/relations/self`] = true;
                        importData[`/tags/${tag}/${person.importId}/relations/self`] = true;
                    });
                }
            });
            admin.database().ref().update(importData).then((value) => {
                return response.status(200).send({
                    errorLog: errorLog
                });
            }).catch((reason) => {
                console.log(reason);
                return response.status(400).send(reason);
            });

        }
        else return response.status(400).send("Nenhuma integração encontrada");
    });

    let validate = (person) => {

        let valid = true;
        let error = { reason: [], person: person };

        if(!person.importId){
            error.reason.push("Pessoa não tem um identificador único");
            valid = false;
        }

        if(!person.displayName){
            error.reason.push("Pessoa não tem o nome completo");
            valid = false;            
        }

        if(!person.gender){
            error.reason.push("Pessoa precisa ter o sexo informado, a informação deve ser 'female' para feminino e 'male' para masculino");
            valid = false;
        }

        if(person.relations){
            for(let relationId in person.relations){
                let relation = person.relations[relationId];
                if(!relation.profile.displayName){
                    error.reason.push("A pessoa possui um relacionamento que não possui nome completo");
                    valid = false;            
                }
        
                if(!relation.profile.gender){
                    error.reason.push("Pessoa possui um relacionamento que não tem o sexo informado, a informação deve ser 'female' para feminino e 'male' para masculino");
                    valid = false;
                }
            }
        }

        if(!valid){
            errorLog.push(error);
        }

        return valid;
    };

}

__users.getProfileAvatar = (request, response)=>{
    const tempFilePath = `${os.tmpdir()}/${request.query.userId}_${request.query.avatar}`;
    console.log(bucket);
    const file = gcs.bucket(config.env.storageBucket).file(`avatars/${request.query.userId}/${request.query.avatar}`);
    file.download({
        destination: tempFilePath
    })
    .then(() => {
        spawn('convert', [tempFilePath, '-thumbnail', '200x200>', tempFilePath])
        .then(()=>{
            let stream = fs.createReadStream(tempFilePath);
            stream.pipe(response);
        });
    })
    .catch((error)=>{
        response.status(400).send(error);
    });
};

module.exports = __users;