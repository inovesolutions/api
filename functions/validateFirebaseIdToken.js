const admin = require('./firebaseAdmin');

module.exports = (req, res, next) => {
  if(req.method === "OPTIONS") return next();
  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
      !req.cookies.__session && (!req.headers.api_key && req.headers.client_id)) {
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    idToken = req.headers.authorization.split('Bearer ')[1];
    verifyFirebaseToken(idToken);
  } else if(req.cookies.__session) {
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
    verifyFirebaseToken(idToken);
  } else if(req.headers.client_id && req.headers.api_key){
    next();
  } else {
    console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
    'Make sure you authorize your request by providing the following HTTP header:',
    'Authorization: Bearer <Firebase ID Token>',
    'or by passing a "__session" cookie.');
    res.status(403).send('Unauthorized');
  }
  
  function verifyFirebaseToken(idToken){
    admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
      req.user = decodedIdToken;
      next();
    }).catch(error => {
      console.error('Error while verifying Firebase ID token:', error);
      res.status(403).send('Unauthorized');
    });
  }
};