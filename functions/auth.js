let functions = require('firebase-functions');
let admin = require('./firebaseAdmin');
let unirest = require('unirest');
let config = require('./config');
var CryptoJS = require("crypto-js");
let password = "OlaEscola123";

const cors = require('cors')({
  origin: true
});

module.exports.verifyPhoneNumber = functions.https.onRequest((request, response) => {
    
    if(request.body.phoneNumber){
         admin.database().ref("users").orderByChild("profile/mobilePhone")
        .equalTo(request.body.phoneNumber.replace(/\D+/g, ""))
        .once("value", (userSnapshot) =>{

            if(userSnapshot.exists()){
                if(Object.keys(userSnapshot.val()).length > 1){
                    return response.status(400).send({
                        code: "400.10",
                        reason: `Existe mais de uma pessoa com esse número de telefone ${request.body.phoneNumber} na nossa base de dados`
                    });
                }
				let code;
                if(request.body.phoneNumber === "15981119127"){
					code = 123456;
                } else{
                    
                	code = Math.floor(Math.random()*90000) + 100000;
                }

                let userId = Object.keys(userSnapshot.val())[0];

                userSnapshot.ref.child(userId).child("authCode").set({
                    value: code,
                    timestamp: new Date().getTime()
                }, (error)=>{

                    let credentials = CryptoJS.AES.encrypt(`${userId}.${code}`, password);

                    var zenviaHeader = {
                        "Authorization": `Basic ${config.zenvia.base64}`,
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    };

                    var zenviaRequest = {
                        "sendSmsRequest": {
                            "from": config.env.appname,
                            "to": `55${request.body.phoneNumber}`,
                            "msg": `Código de ativação: ${code}`,
                            "callbackOption": "NONE"
                        }
                    };

                    unirest.post(config.zenvia.urlSimple)
                    .headers(zenviaHeader)
                    .send(zenviaRequest)
                    .end(function (unirestResponse) {

                        if  (unirestResponse.code === 'ETIMEDOUT'){
                        // timeout de conexao - abre chamado            
                            console.error(`ZenviaTimeout Header: ${JSON.stringify(unirestResponse.headers)}`);
                            console.error(`ZenviaTimeout Body: ${JSON.stringify(unirestResponse.body)}`);
                        }
                        else if (unirestResponse.code >=200 && unirestResponse.code<300){
                            return response.status(200).send({
                                credentials: credentials.toString()
                            });
                        }else{
                            // Problemas nos servidores ou na mensagem, para a operação e grava o chamado
                            console.error(`ZenviaError Header: ${JSON.stringify(unirestResponse.headers)}`);
                            console.error(`ZenviaError Body: ${JSON.stringify(unirestResponse.body)}`);
                        }


                        response.status(400).send({
                            code: "400.20",
                            reason: "Error to send SMS code"
                        });

                    });
                
                });
                
            }
            else{
                response.status(404).send({
                    code: "404.10",
                    reason: `Não encontramos esse número de celular.
                             Verifique se você informou corretamente 
                             ou entre em contato para atualizar seu cadastro`
                });
            }
        });

    }
    else{
        response.status(400).send({
            code: "400.30",
            reason: `Não recebemos o número do celular.
                 Verifique se você digitou corretamente.`
        });
    }
});

module.exports.verifyEmail = functions.https.onRequest((request, response)=>{

    cors(request, response, () => {

        if(request.body.email){

            admin.database().ref("users").orderByChild("profile/email")
            .equalTo(request.body.email)
            .once("value", (userSnapshot) =>{

                if(userSnapshot.exists()){

                    let userId = Object.keys(userSnapshot.val())[0];
                    let user = userSnapshot.val()[userId];
                    
                    if(user.type !== "enterprise"){
                        return response.status(401).json("Usuário sem acesso à área administrativa");
                    }
                    let code = Math.floor(Math.random()*90000) + 100000;

                    userSnapshot.ref.child(userId).child("authCode").set({
                        value: code,
                        timestamp: new Date().getTime()
                    }, (error)=>{

                        let credentials = CryptoJS.AES.encrypt(`${userId}.${code}`, password);

                        unirest.post(config.sendGrid.url)
                        .headers({
                            'Accept': 'application/json', 
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${config.sendGrid.key}`
                        })
                        .send({
                            personalizations:[
                                { to: [{ 
                                    email: request.body.email, name: user.profile.displayName,
                                    substitutions : {
                                        '-name-': user.profile.displayName
                                    }
                                }] }
                            ],
                             from: {
                                email:config.sendGrid.from
                            },
                            subject: `${config.env.appname} - Autenticação`,
                            content: [{
                                type: 'text/html',
                                value: `Olá -name-, este é seu código de ativação ${code}.`
                            }]
                        })
                        .end(function (unirestResponse) {
                            console.log(JSON.stringify(unirestResponse));
                            return response.status(200).send({
                                credentials: credentials.toString()
                            });
                        });
                        
                    });
                }
                else{
                    response.status(404).send({
                        code: "404.10",
                        reason: `Não encontramos esse email.
                                Verifique se você informou corretamente 
                                ou entre em contato para atualizar seu cadastro`
                    });
                }
            });
        }
        else{
            response.status(404).send({
                code: "401.10",
                reason: `O email não foi informado`
            });
        }
    });
});

module.exports.verifyWithCredentials = functions.https.onRequest((request, response)=>{

    cors(request, response, ()=>{
        
        if(!request.body.credentials || !request.body.code){
            return response.status(401).send({
                code: "401.10",
                reason: "Credentials and code is required"
            });
        }

        let code = Number(request.body.code || "0");
        var bytes  = CryptoJS.AES.decrypt(request.body.credentials, password);
        console.log(bytes);
        let decrypted = bytes.toString(CryptoJS.enc.Utf8);
        let credentials = {
            userId: decrypted.split(".")[0],
            code: Number(decrypted.split(".")[1])
        }

        if(credentials.code === code){
            admin.database().ref("users").child(credentials.userId)
            .child("authCode")
            .once("value", (authCodeSnapshot) => {
                if(authCodeSnapshot.exists()){
                    let authCode = authCodeSnapshot.val();
                    let now = new Date().getTime();
                    if(authCode.timestamp + 600000 < now){
                        return response.status(401).send({
                            code: "401.20",
                            reason: "Code has expirated"
                        });
                    }
                    else if(authCode.value === code){
                        admin.auth().createCustomToken(credentials.userId)
                        .then((customToken) => {
                            response.status(200).send({ token: customToken });
                        })
                        .catch((error)=>{
                            response.status(400).send({
                                code: "400.30",
                                reason: "Firebase authentication fail",
                                error: error
                            });
                        });
                    }
                    else{
                        return response.status(401).send({
                            code: "401.30",
                            reason: "Invalid code"
                        });
                    }
                }
                else{
                    return response.status(401).send({
                        code: "401.40",
                        reason: "Invalid code for this credentials"
                    });
                }
            });
        }
        else{
            return response.status(401).send({
                code: "401.50",
                reason: "Código inválido!"
            });
        }
    });

});