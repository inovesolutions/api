let functions = require('firebase-functions');
let gcs = require('@google-cloud/storage')();
let admin = require('./firebaseAdmin');
let bucket = admin.storage().bucket("colegiouirapuru-b9be5.appspot.com");
let config = require('./config');
const plural = require("pluralize-ptbr");

module.exports.onUserTagCreated = functions.database.ref("/users/{userId}/tags/{tagId}")
    .onCreate((event) => {
        let userId = event.params.userId;
        let tagId = event.params.tagId;
        admin.database().ref("users").child(userId).once("value", user => {
            admin.database().ref("tags").child(tagId).child(userId).set(user.val().profile);
        });
    });

module.exports.onUserTagDeleted = functions.database.ref("/users/{userId}/tags/{tagId}")
    .onDelete((event) => {
        let userId = event.params.userId;
        let tagId = event.params.tagId;
        admin.database().ref("tags").child(tagId).child(userId).set(null);
    });

module.exports.onProfileChange = functions.database.ref("/users/{userId}/profile")
    .onWrite((event) => {
        let userId = event.params.userId;
        admin.database().ref("users").child(userId)
            .once("value", (userDataSnapshot) => {
                if (userDataSnapshot.exists()) {
                    let user = userDataSnapshot.val();
                    let updates = {};
                    for (let relationId in user.relations) {
                        let relation = user.relations[relationId];
                        updates[`users/${relationId}/relations/${userId}/profile`] = event.data.val();
                    }

                    for(let tagId in userId.tags){
                        updates[`tags/${tagId}/${userId}`] = event.data.val();
                    }

                    admin.database().ref().update(updates, error => {
                        if (error) console.log(error);
                        else console.log("Atualizações do perfil concluídas");
                    });
                }
            });
    });

module.exports.onRelationCreated =
    functions.database.ref("/users/{userId}/relations/{relationId}/types/{position}")
    .onCreate((event) => {
        let type = event.data.val();
        let updates = {};
        let userId = event.params.userId;
        let relationId = event.params.relationId;
        let position = event.params.position;
        admin.database().ref("users").child(userId).once("value", userSnap => {
            let user = userSnap.val();
            admin.database().ref("users").child(relationId)
                .once("value", relationUserSnap => {
                    let relationUser = relationUserSnap.val();
                    admin.database().ref("relations").child(relationUser.profile.gender).child(type)
                        .child(user.profile.gender).once("value", otherType => {
                            updates[`users/${relationId}/relations/${userId}/profile`] = user.profile;
                            updates[`users/${relationId}/relations/${userId}/types/${position}`] = otherType.val();
                            admin.database().ref().update(updates);
                        });
                });
        });
    });

module.exports.onRelationDeleted =
    functions.database.ref("/users/{userId}/relations/{relationId}/types/{position}")
    .onDelete((event) => {

        let type = event.data.val();
        let updates = {};
        let userId = event.params.userId;
        let relationId = event.params.relationId;
        let position = event.params.position;

        admin.database().ref("users").child(userId)
            .once("value", userSnap => {
                let user = userSnap.val();
                let types = user.relations[relationId].types;
                if (!types) updates[`users/${userId}/relations/${relationId}`] = null;
                let profileRelation = user.relations[relationId].profile;
                admin.database().ref("relations").child(profileRelation.gender).child(type)
                    .child(user.profile.gender).once("value", otherType => {
                        updates[`users/${relationId}/relations/${userId}/types/${position}`] = null;
                        admin.database().ref().update(updates);
                    });
            });
    });