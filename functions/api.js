const functions = require('firebase-functions');
const admin = require('./firebaseAdmin');
const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors');

const validateFirebaseIdToken = require('./validateFirebaseIdToken');

const messages = require('./api.messages');
const __users = require('./api.users');
var bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser);
app.use(validateFirebaseIdToken);
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.post('/messages/post', messages.post);
app.get('/messages/:messageId', messages.getMessage);
app.get('/messages/:tagId/users', messages.getUsers);

app.post('/import/users', __users.datasync);
app.get('/user/is/admin', __users.isAdmin);
app.get('/user/:userId', __users.getUser);
app.post('/users/list', __users.list);

module.exports = functions.https.onRequest(app);
