module.exports = {
    env : {
        appname : "",
        cloudFunctionsUrlBase : "",
        hostingUrlBase: '',
        storageBucket: ""
    },
    sms: {
        url: "http://www.facilitamovel.com.br/api/simpleSend.ft",
        user: "olaescola.lc",
        password: "senhaolaescola",
        short: {
            url: "http://www.facilitamovel.com.br/api/simpleSend.ft",
            user: "",
            password: ""
        },
        status: [
            'Nenhum',
            'Login Invalido',
            'Usuario sem Creditos',
            'Celular Invalido',
            'Campo Mensagem Invalida',
            'Mensagem Agendada',
            'Mensagem enviada'
        ]
    },
    sendGrid:{
        url : 'https://api.sendgrid.com/v3/mail/send',
        key : '',
        from : '',
        sendLimit : 100
    },
    twilio: {
        url: "https://api.twilio.com/2010-04-01/Accounts/AC91d3d989cd8f168fc9eabc5a5ae0cbbe/Messages.json",
        accountSid: "",
        authToken: "",
        from: "+19478889190"
    },
    zenvia: {
        urlSimple: "https://api-rest.zenvia360.com.br/services/send-sms",
        urlMultiple: "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
        base64: "",
        sendLimit: 100
    },
    google:{
        shortener: {
            key: ''
        }
    }
 }