let config = require('./config');

let functions = require('firebase-functions');
let admin = require('./firebaseAdmin');

module.exports.sendFromChat = function(userSenderName, pushIds, message, unread){

    let payload = {
        notification: { title: userSenderName, sound: 'default', badge: ""+unread },
        data: {
            senderName: userSenderName,
            senderId: message.senderId,
            type: message.type,
            collection: "chats",
            content: message.content
        }
    };

    if(message.type === "text"){
        payload.notification.body = message.content;
    }
    else if(message.type == "image"){
        payload.notification.body = "Enviou uma imagem"
    }

    return admin.messaging().sendToDevice(pushIds, payload)
    .then(value =>{ console.log(JSON.stringify(value)); }, 
    error =>{ console.log(JSON.stringify(error)); });

};

module.exports.sendFromMessage = function(pushIds, unread, message){

    return new Promise((resolve, reject) => {
        let data = {
            title: message.content.body.title,
            collection: "messages"
        };
        
        if(message.content.body.subtitle){
            data.subtitle = message.content.body.subtitle;
        }
    
        let payload = {
            notification: { 
                title: `${config.env.appname}`, 
                sound: 'default', badge: "" + unread,
                body: message.content.body.title
            },
            data: data        
        };
    
        admin.messaging().sendToDevice(pushIds, payload)
        .then(value =>{ resolve(value); }, 
        error =>{ reject(error); });
    });
}