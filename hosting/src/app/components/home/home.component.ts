import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import 'firebase/storage';
import {
  AfoListObservable,
  AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Http, Headers } from "@angular/http";
import { SwiperComponent } from "app/modules/swiper/swiper.component";
import { FirebaseDatasource } from 'app/services/firebase-datasource.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { Search } from 'app/pipes/search/search.pipe';
import { UUID } from 'angular2-uuid';
import { LogService } from 'app/services/log.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit, AfterViewInit {
  
  public contactSearch: string = "";
  public chatSearch: string = "";
  public groups:Array<any> = [];
  moduleSelected: any;
  public chatSelected: any;
  public modules:AfoListObservable<any>;
  public showContacts:boolean = false;
  public showWaiting = false;
  
  public avatarDefault = "/assets/images/placeholder_user.png";

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.modules = this.db.list('/modules');
      this.modules.subscribe((modules)=>{
        if(!this.moduleSelected) this.moduleSelected = modules[0];
      });
    })
  }

  constructor(private router: Router, private search:Search,
    private ds:FirebaseDatasource, private http:Http, private logService: LogService,
    private afAuth: AngularFireAuth, private db:AngularFireOfflineDatabase) { 

    let subscription = this.afAuth.authState.subscribe((authUser)=>{
      subscription.unsubscribe();
      if(!authUser || authUser.providerData.length > 0){
        this.router.navigate(["login"]);
      }
      else{
        ds.init(authUser.uid);
      }
    });
  }

  public getRGBColor(rgbValue:any){
    return `rgb(${rgbValue.r}, ${rgbValue.g}, ${rgbValue.b})`;
  }
  
  public setModule(module){
    this.moduleSelected = module;
  }

  public logout(){
    this.afAuth.auth.signOut();
    window.location.reload();
  }

  public searchChat = _.debounce((event) => {
    this.chatSearch = event.target.value;
  }, 500);

  public searchContact = _.debounce((event) => {
    this.contactSearch = event.target.value;
  }, 500);

  public newChat(contact){
    if(this.ds.chats){
      
      this.chatSelected = this.ds.chats.filter((chat)=>{ return chat.$key === contact.$key; })[0];
      if(!this.chatSelected){

        let chat:any = { $key: contact.$key, newChat: true }
        let profile = Object.assign({}, contact);
        delete profile.$key;
        chat.profile = profile;
        chat.messages = {};
        chat.lastChange = new Date().getTime();
        this.ds.chats.push(chat);
        this.ds.chats.sort((a, b)=>{
          return (a.lastChange < b.lastChange)? 1: -1;
        });
        this.chatSelected = this.ds.chats[this.ds.chats.indexOf(chat)];

        if(this.chatSelected.profile.avatar){
          let storageRef = firebase.storage().ref();                
          storageRef.child(`avatars/${this.chatSelected.$key}/${this.chatSelected.profile.avatar}`)
          .getDownloadURL().then((url)=>{ chat.avatarUrl = url; });
        }
      }
    }
    this.showContacts = false;
  }

  public getTagName(tag:String){
    return (tag? tag.replace(/_/g, ' '): "");
  }

  public selectChat(chat){
    this.chatSelected = chat;
  }

  public importUsers(){
    firebase.auth().currentUser.getToken(true)
    .then((idToken)=>{

      const headers = new Headers();
      headers.set('Content-Type', 'application/json; charset=utf-8');
      headers.set("Authorization", `Bearer ${idToken}`);            
      this.http
      .post(`${environment.cloudURL}/api/import/users`,{}, {headers: headers})
      .map(response => response.json())              
      .subscribe((users:any) => {
        console.log(users);
      });

    });
  }

  ngOnInit() { 

  }

}
