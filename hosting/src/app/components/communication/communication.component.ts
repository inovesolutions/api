import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { Search } from 'app/pipes/search/search.pipe';
import { FirebaseDatasource } from 'app/services/firebase-datasource.service';
import { SortPipe } from 'app/pipes/sort/sort.pipe';
import * as _ from 'underscore';
import { Http, Headers } from '@angular/http';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.less']
})
export class CommunicationComponent implements OnInit {

  public communicationSelected: any;
  public communicationSearch:string = "";
  public receiverItems: any[] = [];  
  public receivers: string[];
  public receiverSelected: any;  
  public __showHistory = false;
  
  constructor( private search:Search, public ds:FirebaseDatasource, private sort: SortPipe, private http:Http ) { }

  ngOnInit() {}

  public selectCommunication(communication){
    this.communicationSelected = communication;
    this.receiverItems = [];
    this.receivers = undefined;

    firebase.auth().currentUser.getToken(true).then((idToken) => {
      const headers = new Headers();
      headers.set ('Content-Type', 'application/json; charset=utf-8');
      headers.set ("Authorization", `Bearer ${idToken}`);
      this.http.get(`${environment.cloudURL}/api/messages/${communication.$key}`, { headers: headers })
      .map(response => response.json())              
      .subscribe((data:any) => {
        this.communicationSelected.receivers = data.receivers;
        this.sort.transform(this.communicationSelected.receivers, 'name');
        this.receivers = Object.keys(this.communicationSelected.receivers);
      });
    });
    
    if(this.communicationSelected.content.type === "image"){
      let storageRef = firebase.storage().ref();   
      storageRef.child(`messages/${this.communicationSelected.content.body.content}`)
      .getDownloadURL().then((url)=>{ this.communicationSelected.imageURL = url; });
    }
  }

  public searchReceivers = _.debounce((event) => {
    this.receivers = Object.keys(this.search
    .transform(this.communicationSelected.receivers, event.target.value));
  }, 500);

  public searchCommunication = _.debounce((event) => {
    this.communicationSearch = event.target.value;
  }, 500);

  public showHistory(receiver){
    this.__showHistory = true;
    this.receiverSelected = receiver;
  }

  public getTagName(tag:String){
    return (tag? tag.replace(/_/g, ' '): "");
  }

  public countRead():number{
    return this.receivers.filter(receiverId => {
      let read = false;
      if(this.communicationSelected.receivers[receiverId].appStatus 
        && this.communicationSelected.receivers[receiverId].appStatus.event === "read"){
        read = true;
      } else if(this.communicationSelected.receivers[receiverId].emailStatus 
        && this.communicationSelected.receivers[receiverId].emailStatus.event === "read"){
          read = true;
        }
      return read;
    }).length;
  }

}
