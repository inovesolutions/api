import { Component, OnInit, Input } from '@angular/core';
import { Search } from 'app/pipes/search/search.pipe';
import { SortPipe } from 'app/pipes/sort/sort.pipe';
import * as firebase from 'firebase/app';
import * as _ from 'underscore';
import 'firebase/storage';
import { Subject } from 'rxjs';
import { FirebaseDatasource } from 'app/services/firebase-datasource.service';
import { environment } from 'environments/environment';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {

  public users:Object = {};
  public userSelected: any = {};
  public userItems: any[];
  public onSearchMembers:any;
  public groupSearch = "";
  public waitingRequest = false;
  private limitFirst = 100;
  private startAt = "";

  constructor(private search:Search, private sort:SortPipe, 
    public ds:FirebaseDatasource, private http:Http) { 
      this.users = this.search.transform(this.ds.contacts, "");
    }

  ngOnInit() {
    this.onSearchMembers = _.debounce(event => {
      this.users = this.search.transform(this.ds.contacts, event.target.value);
    }, 500);
  }

  public selectUser(user:any){

    if(this.waitingRequest) return;
    this.waitingRequest = true;
    this.userSelected = user;

    firebase.auth().currentUser.getToken(true).then((idToken) => {
      const headers = new Headers();
      headers.set ('Content-Type', 'application/json; charset=utf-8');
      headers.set ("Authorization", `Bearer ${idToken}`);
      this.http.get(`${environment.cloudURL}/api/user/${user.$key}`, 
      { headers: headers })
      .map(response => response.json())       
      .subscribe((data:any) => {
        this.userSelected.data = data;
        this.waitingRequest = false;
        if(this.userSelected.data.profile.avatar){
          let storageRef = firebase.storage().ref();
          storageRef.child(`avatars/${user.$key}/${this.userSelected.data.profile.avatar}`)
          .getDownloadURL().then((url)=>{ this.userSelected.data.profile.avatarUrl = url; });
        } else {
          this.userSelected.data.profile.avatarUrl = "/assets/images/placeholder_user.png";
        }
      }, error => this.waitingRequest = false);
    });  
  }

}
