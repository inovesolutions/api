import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BadgeComponent implements OnInit {

  @Input()
  public counter:number;
  constructor(private changeDetectorRef: ChangeDetectorRef) {

  }
  ngOnInit() {
    this.changeDetectorRef.markForCheck();
  }

}
