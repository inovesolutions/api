import { Component, OnInit, Input, ChangeDetectorRef, ElementRef, ViewChild, QueryList, ViewChildren, ChangeDetectionStrategy } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { UUID } from 'angular2-uuid';
import { FirebaseDatasource } from 'app/services/firebase-datasource.service';
import * as $ from 'jquery'
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  @ViewChild('chatContent') private chatContent: ElementRef;  
  @ViewChildren('messages') messages: QueryList<any>;  
  public previewImage:boolean = false;
  public message:any = { type: "text", content: "" };
  public imageURL:string;
  private imageFile:any;
  public userId:string;
  public showProgressUpload:boolean;
  public progressUpload:string;
  public chat:any;
  
  constructor(private sanitizer:DomSanitizer, private db:AngularFireOfflineDatabase,
    private changeDetectorRef:ChangeDetectorRef, public ds: FirebaseDatasource, private afAuth: AngularFireAuth) {
      this.afAuth.authState.subscribe((authUser)=>{
        this.userId = authUser.uid;
      });
  }

  @Input()
  set chatSelected(chat:any){
    if(!this.chat || this.chat.$key != chat.$key) this.chat = chat;
    else{
      this.chat.profile = chat.profile;
      this.chat.lastChange = chat.lastChange;

      for(let messageId in chat.messages){
        let message = this.chat.messages[messageId];
        if(message){
          message.received = chat.messages[messageId].received;
          message.read = chat.messages[messageId].read;
        }
        else{
          this.chat.messages[messageId] = chat.messages[messageId];
        }
      }
    }
  };

  ngOnInit() { }

  ngAfterViewInit() {
    let currentSticky = 0;
    let previousSticky = -1;
    let lastScrollTop = 0;
    $('section.main').on('scroll', function () {
      var st = $(this).scrollTop();
      let $stickies = $('.followWrap');
      let current = $stickies[currentSticky];
      let previous = (previousSticky > -1 ? $stickies[previousSticky] : null);
      if (st > lastScrollTop) {
        if ($(current).offset().top <= 0) {
          $(current).find('.followMeBar').addClass('fixed');
          if (previous) $(previous).find('.followMeBar').removeClass('fixed');
          if (currentSticky < ($stickies.length - 1)) {
            currentSticky++;
            previousSticky++;
          }
        }
      } else {
        if ($(current).offset().top > 0) {
          $(current).find('.followMeBar').removeClass('fixed');
          $(previous).find('.followMeBar').addClass('fixed');
          if (currentSticky > 0) {
            currentSticky--;
            previousSticky--;
          }
        }
      }
      lastScrollTop = st
    })
    this.messages.changes.subscribe(()=>{
      try {
        this.chatContent.nativeElement.scrollTop = this.chatContent.nativeElement.scrollHeight;    
      } catch(err) { 
        console.log(err);
      }    
    });
    this.changeDetectorRef.markForCheck();
  }

  public onImageChange(event:any){
    var files = event.srcElement.files;
    if(files && files.length > 0){
      this.message.type = 'image';
      this.previewImage = true;
      this.imageFile = files[0];
      this.imageURL = URL.createObjectURL(this.imageFile);
    }
  }

  public cancelImage(){
    this.message.type = 'text';
    this.previewImage = false;
    this.imageFile = undefined;
    this.imageURL = "";
  }

  public sendMessage(){

    let key = firebase.database().ref(`users/${this.userId}/chats/${this.chat.$key}/messages`).push().key;    
    let m = Object.assign({}, this.message);
    this.message = { type: "text", content: "" };    
    m.date = firebase.database.ServerValue.TIMESTAMP;
    m.sent = true;
    m.deleted = false;
    m.read = false;
    m.received = false;
    m.uploaded = false;    
    m.senderId = this.userId;
  
    if(m.type === "image"){

      this.showProgressUpload = true;
      m.content = UUID.UUID();      
      let storageRef = firebase.storage().ref(`users/${this.chat.$key}/chats/${this.userId}/${key}`)
      .child(m.content);
      storageRef.updateMetadata({
        cacheControl: "max-age=7200"
      });

      let uploadTask = storageRef.put(this.imageFile);
      uploadTask.then((dataSnapshot)=>{ 
        m.uploaded = true;
        this.saveMessage(m, key);
        this.showProgressUpload = false;
        this.previewImage = false;
      });
      uploadTask.on('state_changed', (snapshot:any) => {
        this.progressUpload = `${(snapshot.bytesTransferred / snapshot.totalBytes) * 100}%`;
      });
    }
    else if(m.type === "text"){
      this.saveMessage(m, key);
    }
  }

  public saveMessage(m:any, key:string){
    var updates = {};
    updates[`users/${this.userId}/chats/${this.chat.$key}/messages/${key}`] = m;
    updates[`users/${this.userId}/chats/${this.chat.$key}/profile`] = this.chat.profile;
    updates[`users/${this.userId}/chats/${this.chat.$key}/lastChange`] = firebase.database.ServerValue.TIMESTAMP;
    updates[`users/${this.chat.$key}/chats/${this.userId}/messages/${key}`] = m;
    updates[`users/${this.chat.$key}/chats/${this.userId}/profile`] = this.ds.profile;
    updates[`users/${this.chat.$key}/chats/${this.userId}/lastChange`] = firebase.database.ServerValue.TIMESTAMP;
    firebase.database().ref().update(updates);
  }

  public handleInViewPort(message:any, key:string){
    message.show = true;
    if(message.senderId !== this.userId && !message.read){
      this.db.object(`/users/${this.userId}/chats/${message.senderId}/messages/${key}/read`).set(true);
      this.db.object(`/users/${message.senderId}/chats/${this.userId}/messages/${key}/read`).set(true);
    }
  }

  public showFullImage(message:any){
    this.imageURL = message.imageUrl;
    this.previewImage = true;
  }

  
  public setWriting(event){
    if(event.keyCode === 13) return this.sendMessage();
    if(this.chat.newChat) return;
    this.db.object(`/users/${this.chat.$key}/chats/${this.userId}/writing`).set(true);
  }
}
