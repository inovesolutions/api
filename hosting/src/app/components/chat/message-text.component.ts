import { OnInit, Component, Input } from "@angular/core";
import * as firebase from 'firebase/app';

@Component({
   selector: 'message-text',
   templateUrl: './message-text.component.html',
   styleUrls: ['./message.scss']
 })
 export class MessageTextComponent implements OnInit {

   @Input()
   public message:any;
   public userId = firebase.auth().currentUser.uid;
   ngOnInit(): void { }
} 