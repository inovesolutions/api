import { OnInit, Component, Input } from "@angular/core";
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Component({
   selector: 'message-image',
   templateUrl: './message-image.component.html',
   styleUrls: ['./message.scss']
 })
 export class MessageImageComponent implements OnInit {
  fullImage: boolean;

   @Input()
   public message;
   @Input()
   public contactId:any;

   public userId = firebase.auth().currentUser.uid;
   ngOnInit(): void { 

      let storageRef:firebase.storage.Reference;

      console.log(this.message);
      
      if(this.message.senderId === this.userId){
         storageRef = firebase.storage().ref(`/users/${this.contactId}/chats/${this.userId}`)
         .child(this.message.messageId).child(this.message.content);
      }
      else{
         storageRef = firebase.storage().ref(`/users/${this.userId}/chats/${this.contactId}`)
         .child(this.message.messageId).child(this.message.content);
      }

      storageRef.updateMetadata({
        cacheControl: "max-age=7200"
      });

      storageRef.getDownloadURL().then((url)=>{ this.message.imageUrl = url; });
   }
} ;