import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

import { environment } from '../../../environments/environment';

import { Router } from "@angular/router";
import { Http } from "@angular/http";
import { ToastyService, ToastData } from 'ng2-toasty';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  public showInputCode:boolean = false;
  public showButtonSendCode:boolean = false;
  public showWaiting:boolean = true;
  private authData:any = {};

  public inputCodeConfig:any = {
    placeholder: "Digite o código aqui..",
    style: {
      textAlign: 'center',
      width: '250px',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontSize: '20px',
      color: '#343434',
      transition: 'all 100ms linear',
      fontWeight: 'bold'
    }
  };

  constructor(private afAuth: AngularFireAuth, 
    private toastyService:ToastyService,
    private router:Router, private http: Http) { 

    let subscription = this.afAuth.authState.subscribe((user)=>{

      subscription.unsubscribe();
      
      if(!user){
        this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
      }
      else if(user.providerData.length > 0){
        this.http.post(`${environment.cloudURL}/verifyEmail`, { email: user.email })
        .map(response => response.json())        
        .subscribe((data:any) =>{
          this.authData.credentials = data.credentials;
          this.showInputCode = true;
          this.showWaiting = false;
          this.toastyService.info({
            title: "Pronto!",
            msg: `Enviamos o código de ativação para o email ${user.email}, informe no campo abaixo.`,
            showClose: true,
            timeout: 7000,
            theme: 'default'
          });

        }, (error)=>{ 
          this.showWaiting = false;

          this.toastyService.error({
            title: "Ops!",
            msg: `Não conseguimos fazer a autenticação com o email ${user.email}, você será redirecionado para uma nova tentativa`,
            showClose: true,
            timeout: 7000,
            theme: 'default',
            onRemove: (toast:ToastData) => {
              this.afAuth.auth.signOut();
              this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
            }
          });
        });

      }
      else{ this.router.navigate(['home']); }
      
    });
  }

  ngOnInit() {
  }

  public onCodeChange(code){
    this.authData.code = code;
    if(code.length === 6) this.showButtonSendCode = true;
    else this.showButtonSendCode = false;
  }

  public authenticate(){
    this.showWaiting = true;
    this.http
    .post(`${environment.cloudURL}/verifyWithCredentials`, this.authData)
    .map(response => response.json())
    .subscribe((data:any) =>{
      this.afAuth.auth.signInWithCustomToken(data.token)
      .then((user)=>{
        this.showWaiting = false;
        this.router.navigate(['home']);
      });
    }, (error)=>{
      this.toastyService.error({
        title: "Ops!",
        msg: error,
        showClose: true,
        timeout: 7000,
        theme: 'default'
      });
      this.showWaiting = false;
    });
  }

}
