import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable } from "angularfire2/database";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireAuth } from "angularfire2/auth";
import { Http, Headers } from "@angular/http";
import { UUID } from 'angular2-uuid';

import * as firebase from 'firebase/app';
import 'firebase/storage';
import { SwiperComponent } from "app/modules/swiper/swiper.component";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { TinymceConfig } from './tinymce-config';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import { ToastyService, ToastData } from 'ng2-toasty';
import * as _ from 'underscore';
import { Search } from 'app/pipes/search/search.pipe';
import { SortPipe } from 'app/pipes/sort/sort.pipe';
import { FirebaseDatasource } from 'app/services/firebase-datasource.service';
import { LogService } from 'app/services/log.service';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.less']
})
export class MessageFormComponent implements OnInit, AfterViewInit {

  @ViewChild(SwiperComponent) swiperCompoent:SwiperComponent;
  
  public term:any;
  public user:firebase.User;
  public module:FirebaseObjectObservable<any>;
  public groupsSelected:any[] = [];
  public groupSelected:any;
  public userItems: any[];
  public swiper:any;
  public message:any = { 
    content: { body: { title: "", subtitle: "", imageDescription: "" }, header: {} }
  };
  public imageURL:SafeUrl;
  public imageFile:File;
  public tinymceConfig = new TinymceConfig();
  public showEditor = false;
  public showWaiting = false;
  public usersFromGroup: string[];
  private senderLogId;
  public waitingRequest = false;
  public swiperConfig = {
    pagination: '.swiper-pagination',
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 10,
    keyboardControl: true,
    threshold: 5000
  };

  constructor(private router: Router, public route: ActivatedRoute, private sort:SortPipe,
    private afAuth: AngularFireAuth, private db:AngularFireDatabase, private search:Search,
    public ds:FirebaseDatasource, private sanitizer: DomSanitizer, private http:Http, private toastyService:ToastyService, private logService: LogService) {

      let subscription = this.afAuth.authState.subscribe((user)=>{

        subscription.unsubscribe();
        if(!user || user.providerData.length > 0){
          this.router.navigate(["login"]);
        }
        else{ 
          this.user = user; 
          this.route.params.subscribe((params)=>{
            this.module = this.db.object(`/modules/${params.moduleId}`);

            let subMod = this.module.subscribe((data)=>{
              subMod.unsubscribe();
              this.message.content.header.moduleId = data.$key;
              this.message.content.header.moduleIcon = data.icon;
              this.message.content.header.moduleIconColor = data.iconColor;
              this.message.content.header.moduleIconBackground = data.iconBackground;
              this.message.content.header.moduleIconName = data.iconName;
              this.message.content.header.moduleName = data.name; 
              this.message.content.header.moduleVersion = data.version;            
            });

          });
        }

      });
    }

  ngOnInit() { }

  ngAfterViewInit(): void {
    this.swiper = this.swiperCompoent.getSwiper();
  }

  @Input()
  set slideChange(value){ this.swiper.slideTo(value); }

  @Input()
  set removeAllReceivers(value) {
    for(let userId in this.groupSelected.users){
      this.groupSelected.users[userId].removed = value;
    }
  }

  public allReceiversRemoved(){
    let count = 0;
    for(let userId in this.groupSelected.users){
      if(this.groupSelected.users[userId].removed) count++;
    }
    return count === Object.keys(this.groupSelected.users).length;
  }

  public changeTextMessage(text){
    this.message.content.body.content = text || "";
  }

  public changeImageDescription(text){
    this.message.content.body.imageDescription = text;
  }

  public filterGroups = _.debounce(text =>{
    this.term = text;
  }, 500);

  public addGroup(group:any){

    if(this.waitingRequest) return;
    this.waitingRequest = true;
    let exists = this.groupsSelected.filter((g)=>{
      return g.$key === group.$key;
    })[0];

    if(!exists) {
      this.groupsSelected.push(group);
      firebase.auth().currentUser.getToken(true).then((idToken) => {
        const headers = new Headers();
        headers.set ('Content-Type', 'application/json; charset=utf-8');
        headers.set ("Authorization", `Bearer ${idToken}`);
        this.http.get(`${environment.cloudURL}/api/messages/${group.$key}/users`, 
        { headers: headers })
        .map(response => response.json())       
        .subscribe((data:any) => {
          this.waitingRequest = false;
          group.users = data;
        }, error => this.waitingRequest = false );
      });
    } else this.waitingRequest = false;
  }

  public removeGroup(group:any){

    let index = this.groupsSelected.indexOf(group);

    if(index > -1) {
      if(this.groupSelected === group){
        this.groupSelected = undefined;
      }
      this.groupsSelected.splice(index, 1);
    }

  }

  public selectGroup(group:any){
    this.groupSelected = group;
    this.groupSelected.users = this.sort.transform(this.groupSelected.users, "displayName");
    this.usersFromGroup = Object.keys(this.groupSelected.users);
    this.userItems = [];
  }

  public getTagName(tag:String){
    return tag.replace(/_/g, ' ');
  }

  public getQtdUsers(group:any){
    return Object.keys(group.users).length;
  }

  public onImageChange(event){
    var files = event.srcElement.files;
    if(files && files.length > 0){
      this.imageFile = files[0];
      this.message.content.type = "image";
      this.imageURL = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.imageFile));
    }
  }

  public setMessageType(type){
      this.message.content.type = type;
      if(type === "text"){
        this.showEditor = true;        
      }
  }

  public setShowEditor(value){
    if(!value && this.message.content.body.content == undefined){
      this.message.content.body.content = "";
    }
    this.showEditor = value;
  }

  public sendMessage = _.debounce(() => {

    this.senderLogId = this.logService.addLog("Preparando o comunicado para envio...");
    if(this.message.content.type === "image"){
      
      let uniqueFileName = UUID.UUID();
      let storageRef = firebase.storage().ref("/messages");      
      let task = storageRef.child(uniqueFileName).put(this.imageFile).then((dataSnapshot)=>{
        
        this.message.content.body.content = uniqueFileName;
        this.message.content.header.resources = [
          { downloaded: false, name: uniqueFileName }
        ]
        this.sendMessageToServer(storageRef);
      });
    }
    else if(this.message.content.type === "text"){
      this.message.content.body.content = 
      `<div style="font-family: Helvetica; font-size: 14px;">${this.message.content.body.content}</div>`
      this.sendMessageToServer(null);
    }
  }, 500);

  private sendMessageToServer(storageRef:firebase.storage.Reference) {

    firebase.auth().currentUser.getToken(true).then((idToken) => {

      this.message.receivers = {};
      this.message.receiversRemoved = {};
      this.logService.updateLog(this.senderLogId, "Organizando os destinatários, aguarde...");
      this.groupsSelected.forEach((group)=>{
        if(group.users){
          for(let userId in group.users){
            let user = group.users[userId];
            if(user.removed){
              this.message.receiversRemoved[userId] = {
                name: user.displayName,
                email: user.email,
                pushId: user.pushId,
                group: group.$key
              };
            }
            else if(!this.message.receivers[userId]) {
              let receiver:any = {
                name: user.displayName,
                email: user.email,
                pushId: user.pushId,
                group: group.$key
              }
              this.message.receivers[userId] = receiver;
            }
          }
        }
      });

      const headers = new Headers();
      headers.set ('Content-Type', 'application/json; charset=utf-8');
      headers.set ("Authorization", `Bearer ${idToken}`);

    this.logService.updateLog(this.senderLogId, "Iniciando o envio do comunicado...");
      this.http.post(`${environment.cloudURL}/api/messages/post`, 
      { message: this.message }, { headers: headers })
      .map(response => response.json())              
      .subscribe((data:any) => {
        if(storageRef){
          storageRef.updateMetadata({
            cacheControl: 'public,max-age=3600',
            customMetadata: {
              messageId: data.messageId
            }
          });
        }  

        this.toastyService.success({
          title: "Pronto!", msg: `Sua mensagem foi enviada com sucesso`,
          showClose: true, timeout: 3000,
          onRemove: ()=>{
            this.message = { content: { body: { title: "", subtitle: "" }, header: {} } }; 
            this.router.navigate(["/home"]);
            this.logService.removeLog(this.senderLogId);            
          }
        });
      }, (error) => {
        this.toastyService.error({
          title: "Ops!", msg: `Não foi possível enviar a mensagem`,
          showClose: true, timeout: 5000
        });
        this.logService.removeLog(this.senderLogId);
      });
    });
  }
}
