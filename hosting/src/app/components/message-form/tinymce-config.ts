export class TinymceConfig{

    public editorTextConfig = {
      skin_url: '/assets/tinymce/skins/lightgray',
      theme_url: "/assets/tinymce/themes/modern/theme.min.js",
      external_plugins: {
        "fullscreen": "/assets/tinymce/plugins/fullscreen/plugin.min.js",
        "textcolor": "/assets/tinymce/plugins/textcolor/plugin.min.js",
        "autolink": "/assets/tinymce/plugins/autolink/plugin.min.js"
      },
      theme: "modern",
      inline: false,
      menubar: false,
      statusbar: false,
      plugins: "fullscreen textcolor autolink",
      height : "310px",
      toolbar: `fullscreen | styleselect | bold italic alignleft aligncenter alignright alignjustify`
    }

    public editorImageDescriptionConfig = {
      skin_url: '/assets/tinymce/skins/lightgray',
      theme_url: "/assets/tinymce/themes/modern/theme.min.js",
      content_css : "/assets/tinymce/content.css",
      external_plugins: {
        "fullscreen": "/assets/tinymce/plugins/fullscreen/plugin.min.js",
        "textcolor": "/assets/tinymce/plugins/textcolor/plugin.min.js"
      },
      theme: "modern",
      inline: false,
      menubar:false,
      width: '380px',
      height: '100%',
      statusbar: false,
      toolbar: `h2 h3 | bold italic |
      alignleft aligncenter alignright alignjustify outdent indent`
    }
}