import { Component, OnInit } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { ToastyService } from 'ng2-toasty';
import { Http } from '@angular/http';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.component.html',
  styleUrls: ['./chat-view.component.scss']
})
export class ChatViewComponent implements OnInit {

  public chatSelected:any;
  public error:any;
  public user:firebase.User;

  constructor(private router: Router,
    private http: Http, private toastyService:ToastyService,
    private route: ActivatedRoute,
    private afAuth: AngularFireAuth, private db:AngularFireOfflineDatabase) { 
      let subscription = this.afAuth.authState.subscribe((user)=>{
        subscription.unsubscribe();
          this.route.params.subscribe(params => {
            let token = params['token'];
            let contactId = params['contactId'];
            if(user){
              this.user = user;
              this.loadChat(contactId);
            }
            else{
              firebase.auth().signInWithCustomToken(token)
              .then((user)=>{
                this.user = user;
                this.loadChat(contactId);
              })
              .catch((error:any)=> {
                this.error = error;
              });
            }
          });
      });
    }

  ngOnInit() {
  }

  private loadChat(contactId:String){
    this.db.object(`/users/${this.user.uid}/chats/${contactId}`)
    .subscribe((chat)=>{
      if(chat) { 
        if(!chat.profile) return;
        if(!chat.lastChange) return;
        this.chatSelected = chat;
      }
      else{
        let sub = this.db.object(`/users/${this.user.uid}/contacts/${contactId}`)
        .subscribe((contact:any) => {
          sub.unsubscribe();
          this.newChat(contact);
        });
      }
    });
  }

  public newChat(contact){
      
      this.chatSelected = { $key: contact.$key, newChat: true }
      let profile = Object.assign({}, contact);
      delete profile.$key;
      this.chatSelected.profile = profile;
      this.chatSelected.messages = {};
      this.chatSelected.lastChange = new Date().getTime();

      if(this.chatSelected.profile.avatar){
        let storageRef = firebase.storage().ref();                
        storageRef.child(`avatars/${this.chatSelected.$key}/${this.chatSelected.profile.avatar}`)
        .getDownloadURL().then((url)=>{ this.chatSelected.avatarUrl = url; });
      }
  }


}
