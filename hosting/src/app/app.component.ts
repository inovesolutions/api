import { Component } from '@angular/core';
import { ToastyConfig } from 'ng2-toasty';
import { LogService } from 'app/services/log.service';
import { FirebaseDatasource } from 'app/services/firebase-datasource.service';
import * as firebase from 'firebase/app';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { MessagingService } from 'app/services/messaging.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit{
  
  message: BehaviorSubject<any>;

  title = 'APP/UIRAPURU';  
  constructor(private toastyConfig:ToastyConfig, public logService:LogService, private msgService: MessagingService) { 
    toastyConfig.position = "top-center";
  }

  ngOnInit(): void {
    this.msgService.getPermission();
    this.msgService.receiveMessage();
    this.message = this.msgService.currentMessage;
  }
}
