import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AngularFireOfflineModule } from 'angularfire2-offline';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import {ToastyModule} from 'ng2-toasty';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { InViewportModule } from './modules/inviewport';
import { MomentModule } from './modules/moment';
import { VirtualScrollModule } from './modules/virtual-scroll/virtual-scroll';
import { RecyclerModule } from './modules/recycler/recycler';
import { DateFormatDirective } from './directives/date-format.directive';
import { NgInitDirective } from './directives/ng-init.directive';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';

import { FieldsModule } from './modules/fields/fields.module';
import { SwiperModule } from "./modules/swiper/swiper.module";
import { MessageFormComponent } from './components/message-form/message-form.component';
import { SafeHtmlPipe } from './pipes/safe-html/safe-html.pipe';
import { SearchGroups } from './pipes/search-groups/search-groups.pipe';
import { Search } from './pipes/search/search.pipe';
import { KeysPipe } from './pipes/keys/keys.pipe';
import { DateFormatPipe } from './pipes/dateformat/dateformat.pipe';
import { ChatComponent, MessageImageComponent, MessageTextComponent } from './components/chat';

import { FirebaseDatasource } from './services/firebase-datasource.service';
import { BadgeComponent } from './components/badge/badge.component';
import { ChatViewComponent } from './mobile/chat-view/chat-view.component';
import { CommunicationComponent } from './components/communication/communication.component';
import { UsersComponent } from './components/users/users.component';
import { SortPipe } from './pipes/sort/sort.pipe';
import { LogService } from 'app/services/log.service';
import { MessagingService } from 'app/services/messaging.service';
import { AvatarDirective } from 'app/directives/avatar.directive';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent},
  { path: 'mobile/chat/:contactId/:token', component: ChatViewComponent},
  { path: 'messageForm/:moduleId', component: MessageFormComponent},
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent, DateFormatDirective, LoginComponent, NgInitDirective,
    MessageImageComponent, MessageTextComponent, DateFormatPipe, AvatarDirective,
    HomeComponent, MessageFormComponent, SafeHtmlPipe, SearchGroups, Search, 
    KeysPipe, ChatComponent, BadgeComponent, ChatViewComponent, CommunicationComponent, UsersComponent, SortPipe
  ],
  imports: [
    BrowserModule, AngularFireModule.initializeApp(environment.firebase), FormsModule,
    RouterModule.forRoot(appRoutes, {useHash: false}), LazyLoadImageModule, InViewportModule.forRoot(),
    FieldsModule, BrowserAnimationsModule, AngularFontAwesomeModule,
    SwiperModule, ToastyModule.forRoot(), MomentModule, AngularFireOfflineModule,
    VirtualScrollModule, RecyclerModule, HttpModule
  ],
  providers: [AngularFireAuth, AngularFireDatabase, FirebaseDatasource, Search, SortPipe, LogService, MessagingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
