import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: any | any[], expression?: any, reverse?: boolean): any {
    
    let props = expression.split(".");

    if(value && value instanceof Array){
      return value.sort((a:any, b:any):number=>{
        return SortPipe.getValue(a, props) > SortPipe.getValue(b, props) ? 1 : -1;
      });
    }
    else if(value && value instanceof Object){
      let objectSorted = {};
      let keys = Object.keys(value);

      let arraySorted = keys.sort((a:any, b:any):number=>{
        return SortPipe.getValue(value[a], props) > SortPipe.getValue(value[b], props) ? 1 : -1;
      });

      arraySorted.forEach((key)=>{
        objectSorted[key] = value[key];
      });

      return objectSorted;
    }
  }

  private static getValue(object: any, expression: string[]) {
    for (let i = 0, n = expression.length; i < n; ++i) {
      const k = expression[i];
      if (!(k in object)) {
        return;
      }
      object = object[k];
    }

    return object;
  }

}
