import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'searchGroups', pure: true })
export class SearchGroups implements PipeTransform{
    transform(value: any, term: string): any {
        return (value || []).filter((item) => {
            if (term) {
                let regExp = new RegExp('\\b' + term, 'gi');
                return regExp.test(item.$key);
            } else return true;
        });
    }
}