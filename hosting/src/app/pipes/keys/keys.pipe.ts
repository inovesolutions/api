import { PipeTransform, Pipe, WrappedValue } from "@angular/core";

@Pipe({ name: 'keys', pure:false })
export class KeysPipe implements PipeTransform {

  transform(value:any) : any {
    if(!value) return null;
    return  Object.keys(value);
  }
}