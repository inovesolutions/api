import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'search', pure: true })
export class Search implements PipeTransform{

    transform(value: any, term:string): any {
    
        if(!term) return value;
        if(value && value instanceof Array){
            return value.filter((item) => {
                let regExp = new RegExp('\\b' + term, 'gi');
                return regExp.test(JSON.stringify(item));
            });
        }
        else if(value && value instanceof Object){
            let items = {};
            for(let prop in value){
                let item = value[prop];
                let regExp = new RegExp('\\b' + term, 'gi');
                if(regExp.test(JSON.stringify(item))){
                    items[prop] = item;
                }
            }

            return items;
        }
    }
}