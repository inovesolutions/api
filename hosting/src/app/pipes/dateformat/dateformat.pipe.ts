import { Pipe, PipeTransform } from "@angular/core";
import * as moment from 'moment';

@Pipe({ name: 'dateformat', pure: true })
export class DateFormatPipe implements PipeTransform{
    transform(value: any): any {
      moment.locale('pt-BR');
      let NOW = moment();
      let CURRENT = moment(value);     
      if(NOW.isSame(CURRENT, "days")){
         return CURRENT.format("HH:mm");         
      }
      else if(NOW.subtract(1, "days").isSame(CURRENT, "days")){
         return "ONTEM";         
      }
      else{
         return CURRENT.format("DD/MM/YY");
      }
    }
}