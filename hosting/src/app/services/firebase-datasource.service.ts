import { Injectable, NgZone } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { LogService } from 'app/services/log.service';
import { Http, Headers } from '@angular/http';
import { environment } from 'environments/environment';
import * as _ from 'underscore';
import { SortPipe } from 'app/pipes/sort/sort.pipe';

@Injectable()
export class FirebaseDatasource {

  public profile: any;
  private running = false;
  public chats:Array<any> = [];
  public contacts:Array<any> = [];  
  public contactTags: Array<any> = [];
  public communications:Array<any> = [];
  private uid:string;
  public isAdmin:boolean = false;

  constructor(private db:AngularFireDatabase, private zone:NgZone, private sort: SortPipe,
    private http:Http, private logService: LogService) { }

  public init(uid:string){
    this.uid = uid;
    if(this.running) return;
    this.running = true;

    this.getChats();
    this.getContactsWithTags();
    this.getCommunications();
    this.getContacts();

    firebase.auth().currentUser.getToken(true)
    .then((idToken)=>{
      const headers = new Headers()
      headers.set('Content-Type', 'application/json; charset=utf-8');
      headers.set("Authorization", `Bearer ${idToken}`);            
      this.http
      .get(`${environment.cloudURL}/api/user/is/admin`, {headers: headers})
      .map(response => response.json())         
      .subscribe((isAdmin:any) => {
        this.isAdmin = isAdmin;
      })
    });
    let sub = this.db.object(`users/${uid}/profile`)
    .subscribe((profile)=>{
      this.profile = profile;
    });
    let presenceRef = firebase.database().ref("presence").child(uid);    
    var connectedRef = firebase.database().ref('.info/connected');
    connectedRef.on('value', (snapshot)=>{
      if (snapshot.exists()) {
        this.db.object(`users/${uid}`)
        .subscribe(function (user){
          this.unsubscribe();
          presenceRef.child("timestamp").set(firebase.database.ServerValue.TIMESTAMP);
          presenceRef.child("name").set(user.profile.displayName);
          presenceRef.child("connected").set(true);
        });
      }
    });
    presenceRef.child("timestamp").onDisconnect().set(firebase.database.ServerValue.TIMESTAMP);
    presenceRef.child("connected").onDisconnect().set(false);
  }

  public countNotRead(chat){
    let counter = 0;
    for(let messageId in chat.messages){
      let message = chat.messages[messageId];
      if(message.senderId !== this.uid && !message.read) counter++;
    }
    chat.notRead = counter;
  }

  public getContactsWithTags(){
    let logId = this.logService.addLog("Carregando grupos de contatos...");
    this.db.list(`contacts/${this.uid}/withTags`).subscribe(contactTags => {
      if(contactTags) this.contactTags = contactTags;
      this.logService.removeLog(logId);
    });
  }

  public getContacts(){
    let logId = this.logService.addLog("Carregando contatos de conversas...");
    this.db.list(`users_contacts/${this.uid}`).subscribe(contacts => {
      if(contacts) this.contacts = contacts;
      this.logService.removeLog(logId);
    });
  }

  public getCommunications(){
    let logId = this.logService.addLog("Carregando comunicados...");
    this.db.list(`users/${this.uid}/index/messages`).subscribe(communications => {
      this.communications = communications.reverse();
      this.logService.removeLog(logId);
    });
  }

  public getChats(){
    let logId = this.logService.addLog("Carregando conversas...");
    this.db.database.ref(`/users/${this.uid}/chats`)
    .on('child_added', (data)=>{
      if(!data.child("profile").exists()) return;
      if(!data.child("lastChange").exists()) return;
      let chat:any;
      chat = this.chats.filter(_=>{ return _.$key === data.key; })[0];
      if(!chat){
        chat = { $key: data.key, messages: {} };
        chat.profile = data.val().profile;
        chat.lastChange = data.val().lastChange;
        this.chats.push(chat);        
      }
      else{
        chat.profile = data.val().profile;
        chat.lastChange = data.val().lastChange;
      }

      this.chats.sort((a, b)=>{ return (a.lastChange < b.lastChange)? 1: -1; });
      this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/profile`)
      .on('child_changed', (data)=>{ chat.profile = data.val(); });

      this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/writing`)
      .on('value', (data)=>{ 
        chat.writing = data.val();
        setTimeout(()=>{
          this.db.object(`/users/${this.uid}/chats/${chat.$key}/writing`).set(false);          
        }, 2000); 
      });

      this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/messages`)
      .on('child_added', (data)=>{ 
        let message = data.val();
        if(message.type === "image" && !message.uploaded) return;
        chat.messages[data.key] = message;
        chat.messages[data.key].messageId = data.key;
        this.countNotRead(chat);
        if(!chat.messages[data.key].received && this.uid !== chat.messages[data.key].senderId){
          this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/messages/${data.key}/received`)
          .set(true);
          this.db.database.ref(`/users/${chat.$key}/chats/${this.uid}/messages/${data.key}/received`)
          .set(true);
        }
      });

      this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/messages`)
      .on('child_changed', (data)=>{
        if(!chat.messages[data.key]){
          chat.messages[data.key] = data.val();
          chat.messages[data.key].messageId = data.key;          
          this.countNotRead(chat);
          if(!chat.messages[data.key].received && this.uid !== chat.messages[data.key].senderId){
            this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/messages/${data.key}/received`)
            .set(true);
            this.db.database.ref(`/users/${chat.$key}/chats/${this.uid}/messages/${data.key}/received`)
            .set(true);
          }
        }
        else{
          chat.messages[data.key].read = data.val().read;
          chat.messages[data.key].received = data.val().received;
          chat.messages[data.key].deleted = data.val().deleted;
          this.countNotRead(chat);
        }

      });

      this.db.database.ref(`/users/${this.uid}/chats/${chat.$key}/lastChange`)
      .on('value', (data)=>{
        chat.lastChange = data.val();          
        this.chats.sort((a, b)=>{ return (a.lastChange < b.lastChange)? 1: -1; });
      });

      this.db.object(`presence/${chat.$key}`)
      .subscribe((presence)=>{
        chat.presence = presence;
      });

      this.logService.removeLog(logId);
    });
  }
}
