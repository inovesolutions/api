import { Injectable } from "@angular/core";
import { UUID } from "angular2-uuid";
import * as _ from "underscore";

@Injectable()
export class LogService {
  public logs:any[] = [];

  public addLog(message):string{
    let uid = UUID.UUID();
    this.logs.push({
      id: uid,
      message: message
    });
    return uid;
  }

  public removeLog(uid){
    this.logs = _.filter(this.logs, log => {
      return log.id !== uid;
    });
  }

  public updateLog(uid, message){
    let log = _.filter(this.logs, log => {
      return log.id === uid;
    })[0];
    if(log) log.message = message;
  }

  public addProgress(uid, progress){
    let log = _.filter(this.logs, log => {
      return log.id === uid;
    })[0];
    if(log) log.progress = `${progress}%`;
  }
}