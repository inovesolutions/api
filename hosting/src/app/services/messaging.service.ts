import { Injectable }          from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth }     from 'angularfire2/auth';
import * as firebase from 'firebase';
import 'rxjs/add/operator/take';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MessagingService {
  messaging = firebase.messaging()
  currentMessage = new BehaviorSubject(null);
  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth) { }
  private updateToken(token) {
    this.afAuth.authState.take(1).subscribe(user => {
      if (!user) return;
      this.db.object(`users/${user.uid}/adminPushId`).set(token);
      this.db.object(`users/${user.uid}/profile/adminPushId`).set(token);
    })

    this.messaging.onTokenRefresh(function() {
      this.messaging.getToken()
      .then(function(refreshedToken) {
        console.log('Token refreshed.');
        this.afAuth.authState.take(1).subscribe(user => {
          if (!user) return;
          this.db.object(`users/${user.uid}/adminPushId`).set(token);
          this.db.object(`users/${user.uid}/profile/adminPushId`).set(token);
        })    
      })
      .catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
      });
    });
  }

  getPermission() {
      this.messaging.requestPermission()
      .then(() => {
        console.log('Notification permission granted.');
        return this.messaging.getToken();
      })
      .then(token => {
        console.log(token)
        this.updateToken(token)
      })
      .catch((err) => {
        console.log('Unable to get permission to notify.', err);
      });
    }
    receiveMessage() {
       this.messaging.onMessage((payload) => {
        console.log("Message received. ", payload);
        this.currentMessage.next(payload);
      });
    }
}