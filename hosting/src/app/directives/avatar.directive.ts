import { Directive, Input, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Directive({
  selector: '[avatar]'
})
export class AvatarDirective implements OnInit {
   @Input() user:any;
   @Input() key:string;
   constructor() { }
   ngOnInit(): void {
      if(this.user.avatar){
         let storageRef = firebase.storage().ref()
         .child(`avatars/${this.key}/${this.user.avatar}`);
         storageRef.updateMetadata({
            cacheControl: "max-age=7200"
         });
         storageRef.getDownloadURL().then((url)=>{ this.user.avatarUrl = url; });
      }
   }
}