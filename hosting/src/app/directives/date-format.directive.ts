import { Directive, Input, ElementRef, OnChanges, SimpleChanges } from '@angular/core';

import * as moment from 'moment';

@Directive({
  selector: '[dateFormat]'
})
export class DateFormatDirective implements OnChanges{
  
  @Input('dateFormat') date: string;

  ngOnChanges(changes: SimpleChanges): void {
    this.format();
  }

  constructor(private element: ElementRef) {
      
  }

  private format(){
    var currentDate = moment(this.date);
    var today = moment();
    var beforeYesterday = moment();
    beforeYesterday.subtract(2, "d");

    if(currentDate.isBefore(today, "day") && currentDate.isAfter(beforeYesterday)){
      this.element.nativeElement.innerHTML = "ONTEM";
    }
    else if(currentDate.isSame(today, "day") 
            && currentDate.isSame(today, "month") && currentDate.isSame(today, "year")){
              this.element.nativeElement.innerHTML = moment(this.date).format("HH:mm");
    }
    else{
      this.element.nativeElement.innerHTML = moment(this.date).format("DD/MM");
    }

  }

}
