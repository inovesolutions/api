import { Component, OnInit, OnDestroy, EventEmitter, Output, Input } from '@angular/core';

declare var require: any;
const tinymce = require('tinymce');
const shortid = require("shortid");

@Component({
  selector: 'app-tiny-mce',
  templateUrl: './tiny-mce.component.html',
  styleUrls: ['./tiny-mce.component.less']
})
export class TinyMceComponent implements OnInit, OnDestroy {

  ngOnInit(): void {
  }

  @Input() config:any;
  @Input() content: String;
  @Output() onEditorChange = new EventEmitter<any>();

  private editor;
  public elementId = shortid.generate();

  ngAfterViewInit() {

    if(!this.config) this.config = {};

    this.config.selector = `#${this.elementId}`;
    this.config.setup = editor => {
      this.editor = editor;
      editor.on('change', () => {
        this.onEditorChange.emit(editor.getContent());
      });

      editor.on('paste', (e) => {
          this.onEditorChange.emit(editor.getContent());
      });

      editor.on('cut', (e) => {
          this.onEditorChange.emit(editor.getContent());
      });

    };

    this.config.init_instance_callback = ()=>{
      if(this.content){
        this.editor.setContent(this.content);
      }
    }
    tinymce.init(this.config);
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }

}
