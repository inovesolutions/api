import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextFieldComponent } from './text-field/text-field.component';
import { TinyMceComponent } from './tiny-mce/tiny-mce.component';

@NgModule({
  imports: [ CommonModule ],
  exports: [TextFieldComponent, TinyMceComponent],
  declarations: [TextFieldComponent, TinyMceComponent]
})
export class FieldsModule { }
