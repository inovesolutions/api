import { Component, OnInit, Input, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.less']
})
export class TextFieldComponent implements OnInit {

  @Input() public config:any;
  @Output() public onValueChange = new EventEmitter<any>();
  
  public model:String;
  public style:any;
  public inputConfig:any = {
    style: {},
    placeholder: "",
    type: "text"
  } 

  constructor() { }

  ngOnInit() {
    Object.assign(this.inputConfig, this.config);
  }

  public onModelChange(event){
    this.onValueChange.emit(event);
  }

  public onBlur(){
    this.inputConfig.placeholder = this.config.placeholder;
  }

  public onFocus(){
    this.inputConfig.placeholder = "";
  }
}
