import { NgModule, Component, ContentChildren, QueryList, 
    AfterContentInit, Input, EventEmitter, 
    Output, ElementRef, ViewChild, AfterViewInit, Injectable, IterableDiffers, IterableDiffer
} from "@angular/core";
import { RecyclerDataService } from "app/modules/recycler/recycler-data.service";

@Component({
    selector: "recycler",
    templateUrl: "./recycler.html",
    styleUrls: ["./recycler.scss"]
})
export class RecyclerComponent implements AfterContentInit, AfterViewInit{

    @ContentChildren('loader') loader: QueryList<any>;  
    @ViewChild("scrollView") private scrollView:ElementRef;
    @Input() public sourceName:string;
    @Output() public update:EventEmitter<any> = new EventEmitter();

    private scrollHeight:number = 0;
    private viewPortData:any[] = [];
    private differ:IterableDiffer<any>;
    private stackTop:any[] = [];
    private stackBottom:any[] = [];
    private chunck = 50;

    constructor(private element:ElementRef, private differs:IterableDiffers,
        private recyclerDataService:RecyclerDataService){
            this.differ = differs.find([]).create(null);
    }
    
    ngAfterContentInit(): void {
        this.loader.changes.subscribe((changes)=>{

            if(this.scrollView.nativeElement.clientHeight 
                < (2 * this.element.nativeElement.clientHeight)){
                this.addItems();
                this.update.emit(this.viewPortData);                                                            
            }
            
        });
    }

    ngAfterViewInit(): void {

        let arrayDiff = this.differ
        .diff(this.recyclerDataService.getData(this.sourceName));
        
        if(arrayDiff){

            arrayDiff.forEachAddedItem((record)=>{
                this.stackBottom.push(record.item);
            });

            if(this.stackBottom.length > 0){
                this.addItems();
                this.update.emit(this.viewPortData);                                           
            }
        }

        this.recyclerDataService.onDataChange.subscribe((sourceName)=>{

            let arrayDiff = this.differ
            .diff(this.recyclerDataService.getData(this.sourceName));

            if(arrayDiff){

                arrayDiff.forEachAddedItem((record)=>{
                    this.stackBottom.push(record.item);
                });

                
                if(this.scrollView.nativeElement.clientHeight 
                    < (2 * this.element.nativeElement.clientHeight)){
                    this.addItems();
                }

                arrayDiff.forEachRemovedItem((record)=>{

                    let index = this.viewPortData.indexOf(record.item);
                    if(index > -1){
                        this.viewPortData.splice(this.viewPortData.indexOf(record.item), 1);
                    }

                    index = this.stackBottom.indexOf(record.item);
                    if(index > -1){
                        this.stackBottom.splice(this.stackBottom.indexOf(record.item), 1);
                    }
                });
                    
                this.update.emit(this.viewPortData);                                            
                
            }
        });

        this.element.nativeElement.onscroll = (event)=>{
            console.log(this.element.nativeElement.scrollTop);
            if(this.element.nativeElement.scrollTop > this.loader.first.nativeElement.clientHeight){
                this.stackTop.push(this.viewPortData.shift());
                if(this.stackBottom.length > 0){
                    this.viewPortData.push(this.stackBottom.shift());
                }
                this.update.emit(this.viewPortData);
            }
            else{
                
            }
        }    
    }

    private addItems(){
        if(this.stackBottom.length > 0){
            if(this.stackBottom.length > this.chunck){
                this.viewPortData = this.viewPortData.concat(this.stackBottom.splice(0, this.chunck));
            }
            else{
                this.viewPortData = this.viewPortData.concat(this.stackBottom);
                this.stackBottom = [];
            }
        }
    }
}

@NgModule({
    exports: [RecyclerComponent],
    declarations: [RecyclerComponent],
    providers: [RecyclerDataService]
  })
  export class RecyclerModule {}