import { EventEmitter, Injectable } from "@angular/core";

@Injectable()
export class RecyclerDataService {
    
    private data:any = {};
    public onDataChange:EventEmitter<any> = new EventEmitter();

    constructor(){}
    
    public setData(sourceName:string, value:any[]){
        this.data[sourceName] = value;
        this.onDataChange.emit(sourceName);
    }

    public getData(sourceName:string):any[]{
        return this.data[sourceName] || [];
    }
}