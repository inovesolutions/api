module.exports = {
    stripPrefix: 'dist',
    root: 'dist/',
    staticFileGlobs: [
      'dist/index.html',
      'dist/**.js',
      'dist/**.css',
      'dist/**.{woff2, woff, ttf}',
      'dist/**.svg',
      'dist/assets/**/**.css',
      'dist/assets/**/**.js',
      'dist/assets/**/**.{woff, woff2, ttf}',
      'dist/assets/**/**.{svg, png, jpg}'
    ]
  };